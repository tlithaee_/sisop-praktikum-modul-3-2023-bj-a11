#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    // Untuk menghitung banyaknya ACCESSED yang dilakukan.
    printf ("Banyaknya ACCESSED yang dilakukan:\n"); // mencetak teks 
    system("grep ACCESSED log.txt | wc -l"); // mengeksekusi perintah grep ACCESSED log.txt | wc -l

    // Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
    printf ("\nList seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan:\n"); // mencetak teks
    // mengeksekusi perintah grep 'MOVED\|MADE' log.txt | awk -F 'categorized' '{print "categorized"$NF}' | sed '/^$/d' | sort | uniq -c | awk '{print $2 " " $3 "=" " " $1-1}'
    system("grep 'MOVED\\|MADE' log.txt \
    | awk -F 'categorized' '{print \"categorized\"$NF}' \
    | sed '/^$/d' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");


    // Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.
    printf ("\nBanyaknya total file tiap extension:\n");
    // mengeksekusi perintah grep 'MADE\|MOVED' log.txt | grep -o 'categorized/[^o]*' | sed 's/categorized\///' | sed '/^$/d' | sed 's/ \([^o]*\)//' | sort | uniq -c | awk '{print $2 " " "=" " " $1-1}'
    system("grep 'MADE\\|MOVED' log.txt \
    | grep -o 'categorized/[^o]*' \
    | sed 's/categorized\\///' \
    | sed '/^$/d' \
    | sed 's/ \\([^o]*\\)//' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \" \"=\" \" \" $1-1}'");

    return 0;
}
