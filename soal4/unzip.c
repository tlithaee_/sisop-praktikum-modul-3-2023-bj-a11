#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

int main() {

    pid_t pid = fork();
    if (pid == 0) {
        char *args[] = {"wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }

    // Create a directory for the extracted files
    pid = fork();
    if (pid == 0) {
        char *args[] = {"mkdir", "hehe", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }

    // Extract hehe.zip to the new directory
    pid = fork();
    if (pid == 0) {
        char *args[] = {"unzip", "-d", "hehe", "hehe.zip", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }

    // Remove hehe.zip
    pid = fork();
    if (pid == 0) {
        char *args[] = {"rm", "hehe.zip", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }
}
