#define _GNU_SOURCE
#include <pthread.h>
#include <ctype.h>
#include <math.h> 
#include <string.h>
#include <sys/types.h>
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/stat.h>

#define MAX_LEN 256
#define MAX_PATH 256
#define MAX_EXT 5
#define MAX_DEST_SIZE 475

pthread_mutex_t lock; // mutex untuk mengeksekusi thread secara bersamaan
FILE* log_file; // file log yang akan ditulis oleh setiap thread

struct ThreadArgs { // struct untuk menyimpan argumen thread
    char dirname[MAX_LEN]; // nama direktori yang akan dibuat
    int maxFile; // maksimal file yang dapat diproses
};

//POIN E
void AccessedLog(char folderPath[]){ // fungsi untuk menulis pesan log (accesed) ketika mengakses direktori
    time_t now = time(NULL); // mendapatkan waktu saat ini 
    struct tm *t = localtime(&now); // mendapatkan waktu lokal 
    char logAccess[256] = "ACCESSED "; // pesan log (ACCESSED) yang akan ditulis ke file
    char _time_str[80]; // string untuk menyimpan waktu
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t); // mengubah waktu menjadi string

    strcat(logAccess, folderPath); // menambahkan nama direktori ke pesan log 
    pthread_mutex_lock(&lock); // mengunci mutex agar tidak ada thread lain yang menulis ke file log
    fprintf(log_file, "%s %s\n", _time_str, logAccess); // menulis pesan log ke file
    pthread_mutex_unlock(&lock); // membuka mutex agar thread lain dapat menulis ke file log
}
//POIN E

//POIN E
void MadeLog(char createdDir[]){ // fungsi untuk menulis pesan log (made) ketika membuat direktori
    time_t now = time(NULL); // mendapatkan waktu saat ini
    struct tm *t = localtime(&now); // mendapatkan waktu lokal
    char logMADE[256] = "MADE "; // pesan log (MADE) yang akan ditulis ke file
    char _time_str[80]; // string untuk menyimpan waktu
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t); // mengubah waktu menjadi string

    strcat(logMADE, createdDir); // menambahkan nama direktori ke pesan log
    pthread_mutex_lock(&lock); // mengunci mutex agar tidak ada thread lain yang menulis ke file log
    fprintf(log_file, "%s %s\n", _time_str, logMADE); // menulis pesan log ke file
    pthread_mutex_unlock(&lock); // membuka mutex agar thread lain dapat menulis ke file log
}
//POIN E

//POIN B, C, D
void* directory(void* arg) { // fungsi untuk membuat direktori
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg; // mendapatkan argumen thread
    char* dirname = thread_args->dirname; // mendapatkan nama direktori
    int maxFile = thread_args->maxFile; // mendapatkan maksimal file yang dapat diproses
    char ext[4]; // string untuk menyimpan ekstensi file
    strcpy(ext, dirname); // menyalin nama direktori ke string ekstensi
    char categorized_dirname[MAX_LEN] = "categorized/"; // string untuk menyimpan nama direktori yang akan dibuat
    char new_dirname[MAX_LEN]; // string untuk menyimpan nama direktori yang akan dibuat
    struct stat st; // struct untuk menyimpan informasi file

    strcpy(new_dirname, categorized_dirname); // menyalin nama direktori ke string nama direktori baru  // membuat nama direktori baru dengan menambahkan prefix "categorized/"
    strcat(new_dirname, dirname); // menambahkan nama direktori ke string nama direktori baru

    //hitung banyak file
    char countFile[256]; // string untuk menyimpan banyak file
    char hehe_dir[20] ="files"; // string untuk menyimpan nama direktori yang akan dihitung
    char cmd[256]; // string untuk menyimpan command
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext); // membuat command untuk menghitung banyak file

    FILE* fp; // file stream untuk membaca output dari command // Mengeksekusi command dan membuka stream untuk membaca outputnya
    fp = popen(cmd, "r"); // Mengeksekusi command dan membuka stream untuk membaca outputnya
    if (fp == NULL) { // Jika gagal mengeksekusi command
        printf("Error: Failed to execute command.\n"); // Menampilkan pesan error
    }

    // Membaca output dari stream dan menampilkannya ke layar
    AccessedLog(hehe_dir); // menulis pesan log (accesed) ketika mengakses direktori
    fgets(countFile, sizeof(countFile), fp); // membaca output dari stream dan menyimpannya ke string countFile
    int numFiles = atoi(countFile); // mengubah string countFile menjadi integer dan menyimpannya ke numFiles

    char _write[MAX_LEN]; // string untuk menyimpan command // ascending output
    system("touch buffer.txt"); // membuat file buffer.txt
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles); // membuat command untuk menulis ke file buffer.txt
    system(_write); // mengeksekusi command untuk menulis ke file buffer.txt
    
    int numDir = numFiles / maxFile ;  //menghitung total direktori yang dibuat per ext [@todo, check if maxFile=0] 

    if (numFiles % maxFile > 0) // jika ada sisa file yang belum diproses
        numDir = numDir + 1; // menambahkan 1 direktori

    pclose(fp); // Menutup stream dan mengembalikan nilai 0 (sukses)

    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) { // membuat direktori jika belum ada  //create dir  if numDir == 0
        MadeLog(new_dirname); // menulis pesan log (made) ketika membuat direktori // directory creation was successful, so do something
    }

    for (int i = 0; i < numDir; i++) // membuat direktori sebanyak numDir
    {
         char _dirName[30]; // string untuk menyimpan nama direktori // mendefinisikan variable nama directory dengan suffix sesuai dengan index
         strcpy(_dirName, new_dirname); // menyalin nama direktori ke string nama direktori baru
         if(i > 0) { // jika index lebih dari 0
            char suffix[10]; // string untuk menyimpan suffix nama direktori
            sprintf(suffix, " (%d)", i+1); // membuat suffix nama direktori
            strcat(_dirName, suffix); // menambahkan suffix ke nama direktori
        }       

        if (stat(_dirName, &st) == 0) { // jika direktori sudah ada // memeriksa apakah direktori sudah ada
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName); // menampilkan pesan ke layar
        } else { // jika direktori belum ada
                if (mkdir(_dirName, 0777) == 0) { // membuat direktori jika belum ada  //create dir  if numDir == 0
                    MadeLog(_dirName); // menulis pesan log (made) ketika membuat direktori // directory creation was successful, so do something
                    //move files according to their extensions
                    char command1[600]; // string untuk menyimpan command 
                    char command2[500]; // string untuk menyimpan command 
                    char command3[100]; // string untuk menyimpan command 
                    char logAccessSource[200]; // string untuk menyimpan command
                    char logAccessTarget[100]; // string untuk menyimpan command 

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext); // membuat command untuk menulis pesan log (accessed) ketika mengakses direktori
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName); // membuat command untuk menulis pesan log (accessed) ketika mengakses direktori
                    
                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName); // membuat command untuk memindahkan file
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s \">> log.txt && %s", ext, _dirName, command3); // membuat command untuk menulis pesan log (moved) ketika memindahkan file
                    sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2); // membuat command untuk memindahkan file
                    
                    system (logAccessSource); // mengeksekusi command untuk menulis pesan log (accessed) ketika mengakses direktori
                    system (logAccessTarget); // mengeksekusi command untuk menulis pesan log (accessed) ketika mengakses direktori

                    system(command1); // mengeksekusi command untuk memindahkan file

                } else { // jika gagal membuat direktori
                    perror("Gagal membuat direktori"); // menampilkan pesan error
                    
                }
            }
    }
    return NULL; // mengembalikan nilai NULL
}
//POIN B, C, D

int main() { // fungsi main 
//POIN B, C, D
    log_file = fopen("log.txt", "a"); // membuka file log.txt dengan mode append
    if (log_file == NULL) { // jika gagal membuka file
        perror("Failed to open log file"); // menampilkan pesan error
        exit(1); // keluar dari program
    }

    if (pthread_mutex_init(&lock, NULL) != 0) { // inisialisasi mutex lock // jika gagal menginisialisasi mutex lock
        perror("Failed to initialize mutex lock"); // menampilkan pesan error
        exit(1); // keluar dari program
    }

//MEMBUAT CATEGORIZE DIRECTORY
    char filename[50] = "extensions.txt"; // string untuk menyimpan nama file ekstensi
    char dirname[MAX_LEN]; // string untuk menyimpan nama direktori 
    char other_dirname[MAX_LEN] = "other"; // string untuk menyimpan nama direktori
    FILE *fp; // pointer untuk menyimpan file
    struct stat st; // struct untuk menyimpan informasi file
    pthread_t tid[MAX_LEN]; // array untuk menyimpan thread id
    struct ThreadArgs thread_args[MAX_LEN]; // array untuk menyimpan argumen thread
    int i = 0; // inisialisasi index

    fp = fopen(filename, "r"); // membuka file extensions.txt dengan mode read

    // memeriksa apakah file berhasil dibuka
    if (fp == NULL) { // jika gagal membuka file
        printf("File tidak dapat dibuka.\n"); // menampilkan pesan error
        exit(1); // keluar dari program
    }

    // membuat direktori "categorized" jika belum ada
    if (stat("categorized", &st) != 0) { // memeriksa apakah direktori sudah ada
        mkdir("categorized", 0777); // membuat direktori categorized
        MadeLog("categorized"); // menulis pesan log (made) ketika membuat direktori
    }

    // membuat direktori "other" di dalam "categorized" jika belum ada
    if (stat("categorized/other", &st) != 0) { // memeriksa apakah direktori sudah ada
        MadeLog("categorized/other"); // menulis pesan log (made) ketika membuat direktori
        if (mkdir("categorized/other", 0777) != 0) // membuat direktori other di dalam categorized
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n"); // menampilkan pesan error
    }

    char maxfilename[50] = "max.txt"; // string untuk menyimpan nama file max.txt
    FILE *max; // pointer untuk menyimpan file //open max.txt
    max = fopen(maxfilename, "r"); // membuka file max.txt dengan mode read

    // memeriksa apakah file berhasil dibuka
    if (max == NULL) { // jika gagal membuka file
        printf("File tidak dapat dibuka.\n"); // menampilkan pesan error
        exit(1); // keluar dari program
    }

    int maxFile; // integer untuk menyimpan nilai maksimum file
    fscanf(max, "%d", &maxFile); // membaca nilai maksimum file dari file
    fclose(max); // menutup file max.txt

    while (fgets(dirname, MAX_LEN, fp)) { // membaca nama direktori dari file, satu per satu 
        dirname[strcspn(dirname, "\n")] = 0; // menghilangkan karakter newline di akhir string

        // menghapus spasi di akhir string
        size_t len = strlen(dirname); // menghitung panjang string dirname
        while (len > 0 && isspace(dirname[len - 1])) { // jika karakter terakhir adalah spasi
            len--; // mengurangi panjang string
        }
        dirname[len] = '\0'; // mengakhiri string dengan null

        // membuat direktori lain secara asynchronous dengan menggunakan thread
        strcpy(thread_args[i].dirname, dirname); // menyalin nama direktori ke dalam argumen thread
        thread_args[i].maxFile = maxFile; // menyalin nilai maksimum file ke dalam argumen thread
        pthread_create(&tid[i], NULL, directory, &thread_args[i]); // membuat thread
        i++; // menambahkan index
    }

    for (int j = 0; j < i; j++) { // iterasi sebanyak jumlah thread yang dibuat  // join thread yang masih berjalan
        pthread_join(tid[j], NULL); // join thread
    }

    // move other files to categorized/other
    char cmdOther1[600]; // string untuk menyimpan command
    char cmdOther2[500]; // string untuk menyimpan command
    char cmdOther3[100]; // string untuk menyimpan command
    char otherPath[20] = "categorized/other"; // string untuk menyimpan path direktori other
    char logAccessSource[200]; // string untuk menyimpan command
    char logAccessTarget[100]; // string untuk menyimpan command
//POIN B, C, D

//POIN E
    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt"); // membuat command untuk menulis log
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath); // membuat command untuk menulis log
    
    system(logAccessSource); // menulis log (accessed) ketika mengakses file
    system(logAccessTarget); // menulis log (accessed) ketika mengakses file

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath); // membuat command untuk memindahkan file ke dalam direktori other
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s \">> log.txt && %s", otherPath, cmdOther3); // membuat command untuk menulis log (moved) dan memindahkan file
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2); // membuat command untuk memindahkan file ke dalam direktori other dan menulis log (moved)
    system(cmdOther1); // memindahkan file ke dalam direktori other dan menulis log (moved)

    /*output sort buffer.txt*/
    system("sort buffer.txt"); // mengurutkan isi file buffer.txt
    system("rm -rf buffer.txt"); // menghapus file buffer.txt
//POIN E

//POIN C, E
    /*count how many files in dir categorized/other*/
    DIR *dir; // pointer untuk menyimpan direktori
    struct dirent *ent; // pointer untuk menyimpan file
    int count = 0; // integer untuk menyimpan jumlah file
    if ((dir = opendir (otherPath)) != NULL) { // membuka direktori
        AccessedLog(otherPath); // menulis pesan log (accessed) ketika mengakses direktori
        while ((ent = readdir (dir)) != NULL) { // membaca file di dalam direktori
            if(ent->d_type == DT_REG) { // jika file regular
                count++; // menambahkan jumlah file
            }
        }
        closedir (dir); // menutup direktori
    } else { // jika gagal membuka direktori
        perror ("Tidak bisa membuka direktori"); // menampilkan pesan error
        return 1; // keluar dari program
    }

    printf("extension_other: %d\n", count); // menampilkan jumlah file di dalam direktori other
//POIN C, E

    fclose(fp); // menutup file
    
    pthread_mutex_destroy(&lock); // menghancurkan mutex lock untuk menulis log

    fclose(log_file); // menutup file log

    return 0;
}
