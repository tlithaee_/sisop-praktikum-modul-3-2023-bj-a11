#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <ctype.h>
#include <string.h>
#define MAX_TREE_HT 100

struct MinHeapNode;
struct MinHeap;
struct MinHeapNode* newNode(char data, unsigned freq);
struct MinHeap* createMinHeap(unsigned capacity);
struct MinHeapNode* extractMin(struct MinHeap* minHeap);
struct MinHeap* createAndBuildMinHeap(char data[], int freq[], int size);
struct MinHeapNode* buildHuffmanTree(char data[], int freq[], int size);
void swapMinHeapNode(struct MinHeapNode** a, struct MinHeapNode** b);
void minHeapify(struct MinHeap* minHeap, int idx);
void buildMinHeap(struct MinHeap* minHeap);
void insertMinHeap(struct MinHeap* minHeap, struct MinHeapNode* minHeapNode);
void printCodes(struct MinHeapNode* root, int arr[], int top, char huffman[][26]);
void HuffmanCodes(char huruf[], int freq[], char huffman[][26], int size);
void sort(char arr[], int count[], int low, int high);
int isSizeOne(struct MinHeap* minHeap);
int isLeaf(struct MinHeapNode* root);

int main(){
    char size = 26, c, huffCodes[26][26], huruf[26];
    int sblm = 0, stlh = 0, freq[26], fd1[2], fd2[2];

    for (int i = 0; i < 26; i++) {
        freq[i] = 0;
        huruf[i] = 65 + i;
    }
    if (pipe(fd1) == -1 || pipe(fd2) == -1) {
        exit(1);
    }
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        exit(1);
    }
    else if (child_id > 0){
        FILE *input_file = fopen("file.txt", "r");
        while ((c = fgetc(input_file)) != EOF){
            if (isalpha(c)) {
                freq[toupper(c) - 'A']++;
            }
        }

        write(fd1[1], freq, sizeof(freq));
        close(fd1[1]);
        wait(NULL);

        read(fd2[0], huffCodes, sizeof(huffCodes));
        input_file = fopen("file.txt", "r");
        while ((c = fgetc(input_file)) != EOF){
            if (isalpha(c)) {
                sblm += 8;
                stlh += strlen(huffCodes[toupper(c) - 'A']);
            }
        }
        fclose(input_file);
        printf("Perbandingan :\n");
        printf("Jumlah bit sebelum Huffman : %d bits\n", sblm);
        printf("JUmlah bit setelah Huffman : %d bits\n", stlh);
        close(fd2[0]);
    }
    else {
        read(fd1[0], freq, sizeof(freq));
        close(fd1[0]);

        for (int i = 0; i < 26; i++){
            if (freq[i] == 0) {
                size--;
            }
            for (int j = 0; j < 26; j++) {
                huffCodes[i][j] = NULL;
            }
        }
        sort(huruf, freq, 0, 25);
        HuffmanCodes(huruf, freq, huffCodes, size);

        write(fd2[1], huffCodes, sizeof(huffCodes));
        close(fd2[1]);
        exit(0);
    }
}

void sort(char arr[], int count[], int low, int high) {
    int i, j, temp1;
    char temp2;
    for (i = low; i <= high; i++) {
        for (j = i + 1; j <= high; j++) {
            if (count[j] > count[i]) {
                temp1 = count[i];
                count[i] = count[j];
                count[j] = temp1;
                temp2 = arr[i];
                arr[i] = arr[j];
                arr[j] = temp2;
            }
        }
    }
}
struct MinHeapNode {
    char data;
    unsigned freq;
    struct MinHeapNode *left, *right;
};
struct MinHeap {
    unsigned size;
    unsigned capacity;
    struct MinHeapNode** array;
};
struct MinHeapNode* newNode(char data, unsigned freq)
{
    struct MinHeapNode* temp = (struct MinHeapNode*)malloc(sizeof(struct MinHeapNode));
    temp->left = temp->right = NULL;
    temp->data = data;
    temp->freq = freq;
    return temp;
}
struct MinHeap* createMinHeap(unsigned capacity)
{
    struct MinHeap* minHeap = (struct MinHeap*)malloc(sizeof(struct MinHeap));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = (struct MinHeapNode**)malloc(minHeap->capacity * sizeof(struct MinHeapNode*));
    return minHeap;
}
void swapMinHeapNode(struct MinHeapNode** a, struct MinHeapNode** b)
{
    struct MinHeapNode* t = *a;
    *a = *b;
    *b = t;
}
void minHeapify(struct MinHeap* minHeap, int idx)
{
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;
    if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
        smallest = left;
    if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
        smallest = right;
    if (smallest != idx) {
        swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}
void buildMinHeap(struct MinHeap* minHeap)
{
    int n = minHeap->size - 1;
    int i;
    for (i = (n - 1) / 2; i >= 0; --i)
        minHeapify(minHeap, i);
}
struct MinHeap* createAndBuildMinHeap(char data[], int freq[], int size)
{
    struct MinHeap* minHeap = createMinHeap(size);
    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(data[i], freq[i]);
    minHeap->size = size;
    buildMinHeap(minHeap);
    return minHeap;
}
void insertMinHeap(struct MinHeap* minHeap, struct MinHeapNode* minHeapNode)
{
    ++minHeap->size;
    int i = minHeap->size - 1;
    while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    minHeap->array[i] = minHeapNode;
}
struct MinHeapNode* extractMin(struct MinHeap* minHeap)
{
    struct MinHeapNode* temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
    --minHeap->size;
    minHeapify(minHeap, 0);
    return temp;
}
int isSizeOne(struct MinHeap* minHeap)
{
    return (minHeap->size == 1);
}
struct MinHeapNode* buildHuffmanTree(char data[], int freq[], int size)
{
    struct MinHeapNode *left, *right, *top;
    struct MinHeap* minHeap = createAndBuildMinHeap(data, freq, size);
    while (!isSizeOne(minHeap)) {
        left = extractMin(minHeap);
        right = extractMin(minHeap);

        top = newNode('$', left->freq + right->freq);

        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }
    return extractMin(minHeap);
}
int isLeaf(struct MinHeapNode* root)
{
    return !(root->left) && !(root->right);
}
void printCodes(struct MinHeapNode* root, int arr[], int top, char huffman[][26])
{
    if (root->left) {
        arr[top] = 0;
        printCodes(root->left, arr, top + 1, huffman);
    }
    if (root->right) {
        arr[top] = 1;
        printCodes(root->right, arr, top + 1, huffman);
    }
    if (isLeaf(root)) {
        for (int i = 0; i < top; i++) {
            huffman[root->data - 'A'][i] = arr[i] + '0';
        }
        huffman[root->data - 'A'][top] = '\0';
    }
}
void HuffmanCodes(char huruf[], int freq[], char huffman[][26], int size)
{
    struct MinHeapNode* root = buildHuffmanTree(huruf, freq, size);
    int arr[MAX_TREE_HT], top = 0;
    printCodes(root, arr, top, huffman);
}
