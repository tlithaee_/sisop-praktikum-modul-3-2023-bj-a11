#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#define ROW_A 4
#define COL_A 2
#define ROW_B 2
#define COL_B 5

void print_matrix(int row, int col, int matrix[row][col]){
    for(int i=0; i<row; i++){
        for(int j=0; j<col; j++){
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

int main(){
    int matrix_a[ROW_A][COL_A];
    int matrix_b[ROW_B][COL_B];
    int result[ROW_A][COL_B];

    // generate random values for matrix a
    srand(time(NULL));
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_A; j++){
            matrix_a[i][j] = (rand() % 5) + 1;
        }
    }

    // generate random values for matrix b
    for(int i=0; i<ROW_B; i++){
        for(int j=0; j<COL_B; j++){
            matrix_b[i][j] = (rand() % 4) + 1;
        }
    }

    // print matrix a
    printf("Matrix A:\n");
    print_matrix(ROW_A, COL_A, matrix_a);
    printf("\n");

    // print matrix b
    printf("Matrix B:\n");
    print_matrix(ROW_B, COL_B, matrix_b);
    printf("\n");

    // multiply matrix a and matrix b
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_B; j++){
            int sum = 0;
            for(int k=0; k<COL_A; k++){
                sum += matrix_a[i][k] * matrix_b[k][j];
            }
            result[i][j] = sum;
        }
    }

    // print result matrix
    printf("Result:\n");
    print_matrix(ROW_A, COL_B, result);

    // create shared memory for result matrix
    key_t key = 1234;
    int shmid = shmget(key, sizeof(result), IPC_CREAT | 0666);
    unsigned long long int (*shm_ptr)[COL_B] = shmat(shmid, NULL, 0);

    // write result matrix to shared memory
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_B; j++){
            shm_ptr[i][j] = result[i][j];
        }
    }

    sleep(10);

    // detach and delete shared memory
    shmdt(shm_ptr);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
