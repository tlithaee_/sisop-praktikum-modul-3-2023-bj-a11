#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW_A 4
#define COL_A 2
#define ROW_B 2
#define COL_B 5

// function to calculate factorial
void factorial(int n, char *result){
    int res[200] = {0};
    res[0] = 1;
    int len = 1;
    for(int i=2; i<=n; i++){
        int carry = 0;
        for(int j=0; j<len; j++){
            int mul = res[j] * i + carry;
            res[j] = mul % 10;
            carry = mul / 10;
        }
        while(carry){
            res[len] = carry % 10;
            carry /= 10;
            len++;
        }
    }
    for(int i=len-1, j=0; i>=0; i--, j++){
        result[j] = res[i] + '0';
    }
}

int main(){
    // get shared memory for result matrix
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int[ROW_A][COL_B]), 0666);
    unsigned long long int (*shm_ptr)[COL_B] = shmat(shmid, NULL, 0);

    // print result matrix from shared memory
    printf("Result:\n");
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_B; j++){
            printf("%llu ", shm_ptr[i][j]);
        }
        printf("\n");
    }

    // calculate factorial of each element in matrix
    printf("\nFactorial:\n");
    char result[200];
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_B; j++){
            factorial(shm_ptr[i][j], result);
            printf("%s ", result);
        }
        printf("\n");
    }

    // detach and remove shared memory
    shmdt(shm_ptr);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
