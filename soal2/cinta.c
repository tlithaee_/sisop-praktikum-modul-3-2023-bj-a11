#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROW_A 4
#define COL_A 2
#define ROW_B 2
#define COL_B 5

// struct to pass arguments to thread function
struct arg_struct {
    int row;
    int col;
    unsigned long long int val;
};

// function to calculate factorial
void *factorial(void *arguments){
    struct arg_struct *args = arguments;
    unsigned long long int val = args->val;
    unsigned long long int res[200] = {0};
    res[0] = 1;
    int len = 1;
    for(int i=2; i<=val; i++){
        int carry = 0;
        for(int j=0; j<len; j++){
            int mul = res[j] * i + carry;
            res[j] = mul % 10;
            carry = mul / 10;
        }
        while(carry){
            res[len] = carry % 10;
            carry /= 10;
            len++;
        }
    }
    char *result = (char*)malloc((len + 1) * sizeof(char));
    for(int i=len-1, j=0; i>=0; i--, j++){
        result[j] = res[i] + '0';
    }
    result[len] = '\0';
    return (void*)result;
}

int main(){
    // get shared memory for result matrix
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int[ROW_A][COL_B]), 0666);
    unsigned long long int (*shm_ptr)[COL_B] = shmat(shmid, NULL, 0);

    // print result matrix from shared memory
    printf("Result:\n");
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_B; j++){
            printf("%llu ", shm_ptr[i][j]);
        }
        printf("\n");
    }

    // calculate factorial of each element in matrix using threads
    printf("\nFactorial:\n");
    pthread_t threads[ROW_A * COL_B];
    int thread_count = 0;
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_B; j++){
            struct arg_struct *args = (struct arg_struct*)malloc(sizeof(struct arg_struct));
            args->row = i;
            args->col = j;
            args->val = shm_ptr[i][j];
            pthread_create(&threads[thread_count], NULL, factorial, (void*)args);
            thread_count++;
        }
    }
    for(int i=0; i<ROW_A*COL_B; i++){
        void *result;
        pthread_join(threads[i], &result);
        printf("%s ", (char*)result);
        free(result);
        if((i+1) % COL_B == 0){
            printf("\n");
        }
    }

    // detach and remove shared memory
    shmdt(shm_ptr);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
