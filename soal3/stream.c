#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <json-c/json.h>
#include <semaphore.h>
#include <ctype.h>
//gcc -o stream stream.c -ljson-c

#define MAX_SIZE 1024
#define MAX_USERS 2
void update_playlist();

struct message {
  long message_type;
  char message_text[MAX_SIZE];
  int user_pid; // menambahkan variabel sender_id
};

// Fungsi untuk mendecrypt dengan rot13
char* rot13_decode(const char* str) {
    int i = 0;
    char* output = strdup(str);
    while (output[i] != '\0') {
        if (isalpha(output[i])) {
            if ((output[i] >= 'a' && output[i] <= 'm') || (output[i] >= 'A' && output[i] <= 'M')) {
                output[i] += 13;
            } else if ((output[i] >= 'n' && output[i] <= 'z') || (output[i] >= 'N' && output[i] <= 'Z')) {
                output[i] -= 13;
            }
        }
        i++;
    }
    return output;
}

// Fungsi untuk mendecrypt dengan base64
char* base64_decode(const char* input) {
    size_t input_length = strlen(input);
    size_t output_length = (input_length / 4) * 3;
    if (input[input_length - 1] == '=') {
        output_length--;
    }
    if (input[input_length - 2] == '=') {
        output_length--;
    }

    char* output = (char*)malloc(output_length + 1);
    if (output == NULL) {
        return NULL;
    }

    const char* base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    int j = 0;
    for (int i = 0; i < input_length; i += 4) {
        int a = strchr(base64_chars, input[i]) - base64_chars;
        int b = strchr(base64_chars, input[i + 1]) - base64_chars;
        int c = strchr(base64_chars, input[i + 2]) - base64_chars;
        int d = strchr(base64_chars, input[i + 3]) - base64_chars;

        uint32_t buffer = (a << 18) | (b << 12) | (c << 6) | d;

        if (input[i + 2] == '=') {
            output[j++] = (buffer >> 16) & 0xff;
        } else if (input[i + 3] == '=') {
            output[j++] = (buffer >> 16) & 0xff;
            output[j++] = (buffer >> 8) & 0xff;
        } else {
            output[j++] = (buffer >> 16) & 0xff;
            output[j++] = (buffer >> 8) & 0xff;
            output[j++] = buffer & 0xff;
        }
    }

    output[output_length] = '\0';
    return output;
}

// Fungsi untuk mendecrypt dengan hex
char* hex_decode(const char* str) {
    int len = strlen(str);
    char* output = (char*)calloc((len / 2) + 1, sizeof(char));
    char byte_str[3] = {'\0', '\0', '\0'};
    int j = 0;

    for (int i = 0; i < len; i += 2) {
        byte_str[0] = str[i];
        byte_str[1] = str[i + 1];
        output[j++] = strtol(byte_str, NULL, 16);
    }

    return output;
}

int compare_strings(const void* a, const void* b) {
    const char** string_a = (const char**) a;
    const char** string_b = (const char**) b;
 
    return strcasecmp(*string_a, *string_b);
}

void decrypt(const char *input_file, const char *output_file) {
    FILE *fp;
    FILE *out_fp;
    char buffer[100000];

    struct json_object *parsed_json;
    struct json_object *method;
    struct json_object *song;

    size_t n_objects;
    size_t i;

    fp = fopen(input_file, "r");
    fread(buffer, 100000, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);

    if (parsed_json == NULL) {
        fprintf(stderr, "Error: failed to parse JSON\n");
        return;
    }

    out_fp = fopen(output_file, "w");

    if (json_object_is_type(parsed_json, json_type_array)) {
        n_objects = json_object_array_length(parsed_json);

        char **decrypted_songs = (char **) malloc(n_objects * sizeof(char *));

        for (i = 0; i < n_objects; i++) {
            struct json_object *obj = json_object_array_get_idx(parsed_json, i);

            json_object_object_get_ex(obj, "method", &method);
            json_object_object_get_ex(obj, "song", &song);

            char *decrypted_song;
            if (strcmp(json_object_get_string(method), "rot13") == 0) {
                decrypted_song = rot13_decode(strdup(json_object_get_string(song)));
            } else if (strcmp(json_object_get_string(method), "base64") == 0) {
                decrypted_song = base64_decode(strdup(json_object_get_string(song)));
            } else if (strcmp(json_object_get_string(method), "hex") == 0) {
                decrypted_song = hex_decode(strdup(json_object_get_string(song)));
            } else {
                decrypted_song = strdup(json_object_get_string(song));
            }

            decrypted_songs[i] = decrypted_song;
        }

        qsort(decrypted_songs, n_objects, sizeof(char *), compare_strings);

        for (i = 0; i < n_objects; i++) {
            fprintf(out_fp, "%s\n", decrypted_songs[i]);
            free(decrypted_songs[i]);
        }

        free(decrypted_songs);
    }

    fclose(out_fp);
}

void print_playlist() {
    FILE* playlist_fp = fopen("playlist.txt", "r");
    if (playlist_fp == NULL) {
        perror("Error: failed to open playlist file");
        return;
    }

    char line[MAX_SIZE];
    while (fgets(line, sizeof(line), playlist_fp)) {
        printf("%s", line);
    }

    fclose(playlist_fp);
}

char *strcasestr(const char *haystack, const char *needle)
{
    size_t needle_len = strlen(needle);

    if (needle_len == 0) {
        return (char *)haystack;
    }

    for (; *haystack; ++haystack) {
        if (strncasecmp(haystack, needle, needle_len) == 0) {
            return (char *)haystack;
        }
    }

    return NULL;
}

void play(char *song_name, pid_t user_pid) {
    char *start_quote = strchr(song_name, '"');
    if (start_quote == NULL) {
        printf("Error: invalid song name format\n");
        return;
    }
    char *end_quote = strchr(start_quote + 1, '"');
    if (end_quote == NULL) {
        printf("Error: invalid song name format\n");
        return;
    }
    strncpy(song_name, start_quote + 1, end_quote - start_quote - 1);
    song_name[end_quote - start_quote - 1] = '\0';

    FILE *playlist_fp = fopen("playlist.txt", "r");
    if (playlist_fp == NULL) {
        perror("Error: failed to open playlist file");
        return;
    }

    char line[MAX_SIZE];
    int n_songs = 0;

    while (fgets(line, sizeof(line), playlist_fp)) {
        if (strcasestr(line, song_name)) {
            n_songs++;
        }
    }

    fclose(playlist_fp);

    if (n_songs == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song_name);
    } else if (n_songs == 1) {
        playlist_fp = fopen("playlist.txt", "r");
        if (playlist_fp == NULL) {
            perror("Error: failed to open playlist file");
            return;
        }
        while (fgets(line, sizeof(line), playlist_fp)) {
            if (strcasestr(line, song_name)) {
            printf("USER %d PLAYING %s", (int)user_pid, line);
                break;
            }
        }
        fclose(playlist_fp);
    } else {
        playlist_fp = fopen("playlist.txt", "r");
        if (playlist_fp == NULL) {
            perror("Error: failed to open playlist file");
            return;
        }
        int song_number = 1;
        while (fgets(line, sizeof(line), playlist_fp)) {
            if (strcasestr(line, song_name)) {
                printf("%d. %s", song_number, line);
                song_number++;
            }
        }
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\"\n", n_songs, song_name);
        fclose(playlist_fp);
    }
}

void add_song_to_playlist(char *song_name, pid_t user_pid) {
    FILE *playlist_fp = fopen("playlist.txt", "r+");
    if (playlist_fp == NULL) {
        perror("Error: failed to open playlist file");
        return;
    }

    char line[MAX_SIZE];
    while (fgets(line, sizeof(line), playlist_fp)) {
        if (strcasestr(line, song_name)) {
            printf("SONG ALREADY ON PLAYLIST\n");
            fclose(playlist_fp);
            return;
        }
    }

    fseek(playlist_fp, 0, SEEK_END);
    fprintf(playlist_fp, "%s\n", song_name);

    fclose(playlist_fp);

    printf("USER %d ADD %s\n", (int)user_pid, song_name);

    update_playlist();
}

void update_playlist() {
    const char* filename = "playlist.txt";
    const int MAX_LINES = 1500;
    char* lines[MAX_LINES];
    int num_lines = 0;

    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Error: Could not open file %s\n", filename);
        return;
    }
    char buffer[100000];
    while (fgets(buffer, sizeof(buffer), file) != NULL) {
        buffer[strcspn(buffer, "\r\n")] = '\0'; 
        lines[num_lines] = strdup(buffer); 
        num_lines++;
    }
    fclose(file);

    qsort(lines, num_lines, sizeof(char*), compare_strings);

    file = fopen(filename, "w");
    if (file == NULL) {
        printf("Error: Could not open file %s\n", filename);
        return;
    }
    for (int i = 0; i < num_lines; i++) {
        fprintf(file, "%s\n", lines[i]);
        free(lines[i]); 
    }
    fclose(file);
}


int main() {
    pid_t active_users[MAX_USERS];
    int active_users_count = 0;
    struct message message;
    key_t key = ftok("stream.c", 'a');
    int msgid = msgget(key, 0666 | IPC_CREAT);
    if (msgid == -1) {
        printf("Error: cannot create message queue\n");
    exit(1);
    }

    sem_t *sem;
    if ((sem = sem_open("/semaphoresaya", O_CREAT, 0666, MAX_USERS)) == SEM_FAILED) {
        perror("sem_open");
        exit(1);
    }

    printf("Stream is waiting for messages... (\"QUIT\" to exit)\n");

    while (1) {
    //3A sistem stream (receiver) stream.c menggunakan message queue (wajib).
        if (msgrcv(msgid, &message, sizeof(message) - sizeof(long), 0, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }

        int found_user = 0;
        for (int i = 0; i < active_users_count; i++) {
            if (active_users[i] == message.user_pid) {
                found_user = 1;
                break;
            }
        }

        if (!found_user && active_users_count == MAX_USERS) {
            printf("STREAM SYSTEM OVERLOAD: User %d cannot send commands at the moment.\n", message.user_pid);
            continue;
        }

        if (!found_user) {
            if (sem_wait(sem) == -1) {
                perror("sem_wait");
                exit(1);
            }
            active_users[active_users_count++] = message.user_pid;
        }

        //3B perintah DECRYPT untuk decrypt file song-playlist.json sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet
        if (strcmp(message.message_text, "DECRYPT") == 0) {
            decrypt("song-playlist.json", "playlist.txt");
        } 
        //3C perintah LIST agar sistem stream dapat menampilkan daftar lagu yang telah di-decrypt
        else if (strcmp(message.message_text, "LIST") == 0) {
            print_playlist();
            } else if (strcmp(message.message_text, "quit") == 0) {
            int found_user = 0;
            for (int i = 0; i < active_users_count; i++) {
                if (active_users[i] == message.user_pid) {
                    found_user = 1;

                    for (int j = i; j < active_users_count - 1; j++) {
                        active_users[j] = active_users[j + 1];
                    }
                    active_users_count--;
                    break;
                }
            }

            if (found_user) {
                printf("User %d has quit.\n", message.user_pid);
            } else {
                printf("User %d is not active.\n", message.user_pid);
            }
        } 
        //3D perintah PLAY <SONG> dengan ketentuan yang telah ditentukan
        else if (strncmp(message.message_text, "PLAY \"", 6) == 0) {
            char song_name[MAX_SIZE];
            strcpy(song_name, message.message_text + 4);

            play(song_name, message.user_pid);
        } 
        //3E menambahkan lagu ke dalam playlist dengan perintah ADD sesuai dengan syarat yang sudah ditentukan
        else if (strncmp(message.message_text, "ADD ", 4) == 0) {
            char song_name[MAX_SIZE];
            strcpy(song_name, message.message_text + 4);

            add_song_to_playlist(song_name, message.user_pid); 
        } 
        else if (strcmp(message.message_text, "QUIT") == 0) {
            if (msgctl(msgid, IPC_RMID, NULL) == -1) {
                perror("msgctl");
                exit(1);
            }

            if (sem_destroy(sem) == -1) {
                perror("sem_destroy");
                exit(1);
            }

            exit(0);
        } 
        //3G apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND"
        else {
            printf("UNKNOWN COMMAND: %s\n", message.message_text);
        }

        if (!found_user) {
            if (sem_post(sem) == -1) {
                perror("sem_post");
                exit(1);
            }
        }
    }

    sem_close(sem);
    sem_unlink("/semaphoresaya");


return 0;
}
