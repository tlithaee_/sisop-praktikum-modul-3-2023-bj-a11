#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <unistd.h> // library ini digunakan untuk memanggil fungsi getpid()
#include <errno.h>

#define MAX_SIZE 1024

struct message {
  long message_type;
  char message_text[MAX_SIZE];
  int user_pid; // menambahkan variabel sender_id
};

int main() {
  key_t key = ftok("stream.c", 'a');
  int msgid = msgget(key, 0666 | IPC_CREAT);
  if (msgid == -1) {
    printf("Error: cannot create message queue\n");
    exit(1);
  }
  struct message message;

  char input[MAX_SIZE];
    int quit = 0;
    while (!quit) {
        printf("Enter message (\"quit\" to exit): ");
        fgets(input, MAX_SIZE, stdin);
        input[strcspn(input, "\n")] = '\0'; 
        if (strncmp(input, "ADD ", 3) == 0 || strncmp(input, "DECRYPT", 7) == 0 || strncmp(input, "LIST", 4) == 0 || strncmp(input, "PLAY ", 4) == 0 || strcmp(input, "quit") == 0|| strcmp(input, "QUIT") == 0) {
                pid_t user_pid = getpid();
                message.user_pid = user_pid;
                strncpy(message.message_text, input, MAX_SIZE);

            } else {
                message.user_pid = 0; 
                strcpy(message.message_text, input);
            }

            message.message_type = 1;

            if (msgsnd(msgid, &message, sizeof(message) - sizeof(long), 0) == -1) {
                perror("msgsnd");
                exit(1);
            }

            if (strcmp(input, "quit") == 0) {
                quit = 1;
                printf("Exiting program...\n");
                exit(0);
            }
        }
        
  return 0;
}
