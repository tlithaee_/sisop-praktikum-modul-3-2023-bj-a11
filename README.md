# Praktikum Sistem Operasi
# Modul 3
### Kelompok A11
### Anggota :
|            Nama           |     NRP    |
| ------                    | ------     |
| Frederick Yonatan Susanto | 5025211121 |
| Fathin Muhashibi Putra    | 5025211229 |
| Syarifah Talitha Erfany   | 5025211175 |

## No. 1 
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

### a. 
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

```
FILE *input_file = fopen("file.txt", "r");
while ((c = fgetc(input_file)) != EOF){
    if (isalpha(c)) {
        freq[toupper(c) - 'A']++;
    }
}

write(fd1[1], freq, sizeof(freq));
close(fd1[1]);
```
#### Penjelasan :
- `FILE *input_file = fopen("file.txt", "r");` : Membuka file dengan nama "file.txt" menggunakan     fungsi fopen() dengan mode "r" (read) dan menampung file pointer ke variabel input_file.

- `while ((c = fgetc(input_file)) != EOF){` :
Membaca setiap karakter dari file menggunakan fungsi fgetc() dan menampungnya ke variabel c. Loop akan berjalan selama c bukanlah akhir dari file (EOF).

- `if (isalpha(c)) {` :
Memeriksa apakah karakter c adalah sebuah huruf alfabet menggunakan fungsi isalpha().

- `freq[toupper(c) - 'A']++;` :
Mengkonversi huruf c menjadi huruf besar menggunakan fungsi toupper() dan menghitung frekuensi kemunculan huruf tersebut dengan menambahkan nilai di indeks yang sesuai di array freq.

- `write(fd1[1], freq, sizeof(freq));` :
Menulis array freq ke dalam fd1[1] menggunakan fungsi write(). fd1[1] merupakan file descriptor yang merepresentasikan ujung tulis (write end) dari sebuah pipe.

- `close(fd1[1]);` :
Menutup ujung tulis (write end) dari pipe dengan memanggil fungsi close().

### b. 
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

```
read(fd1[0], freq, sizeof(freq));
close(fd1[0]);

for (int i = 0; i < 26; i++){
    if (freq[i] == 0) {
        size--;
    }
    for (int j = 0; j < 26; j++) {
        huffCodes[i][j] = NULL;
    }
}
sort(huruf, freq, 0, 25);
HuffmanCodes(huruf, freq, huffCodes, size);
```
#### Penjelasan :
- `read(fd1[0], freq, sizeof(freq))` : membaca data dari file descriptor fd1 pada indeks 0 dan memasukkannya ke dalam array freq dengan ukuran yang ditentukan oleh sizeof(freq).

- `close(fd1[0])` : menutup file descriptor pada indeks 0.

- `for (int i = 0; i < 26; i++)` : melakukan loop dari 0 hingga 25 untuk setiap indeks array freq dan huffCodes.

- `if (freq[i] == 0) {size--;}` : mengurangi nilai variabel size jika nilai dari freq[i] sama dengan 0.

- `for (int j = 0; j < 26; j++) {huffCodes[i][j] = NULL;}` : mengisi setiap elemen dari array huffCodes dengan nilai NULL.

- `sort(huruf, freq, 0, 25);` : mengurutkan elemen array huruf dan freq menggunakan fungsi sort() yang dijelaskan sebelumnya.

- `HuffmanCodes(huruf, freq, huffCodes, size);` : memanggil fungsi HuffmanCodes() untuk membangun tree Huffman berdasarkan array huruf, freq, dan huffCodes.

### c. 
Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

```
write(fd2[1], huffCodes, sizeof(huffCodes));
close(fd2[1]);
```
#### Penjelasan :
- `write(fd2[1], huffCodes, sizeof(huffCodes));` :
Baris ini menulis data array huffCodes ke file descriptor output fd2[1]. fd2[1] adalah bagian dari file descriptor yang dibuat menggunakan pipe() sebelumnya dan digunakan untuk mengirimkan data dari proses ini ke proses lain. huffCodes adalah array dua dimensi yang berisi kode Huffman yang dihasilkan dari proses sebelumnya.

- `close(fd2[1]);` :
Baris ini menutup file descriptor output fd2[1] setelah selesai menulis data. Ini menandakan bahwa proses ini telah selesai mengirim data dan proses lain dapat membaca data dari file descriptor input yang terkait.

### d. 
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 

```
read(fd2[0], huffCodes, sizeof(huffCodes));
input_file = fopen("file.txt", "r");
while ((c = fgetc(input_file)) != EOF){
    if (isalpha(c)) {
        sblm += 8;
        stlh += strlen(huffCodes[toupper(c) - 'A']);
    }
}
fclose(input_file);

```
#### Penjelasan :
- `read(fd2[0], huffCodes, sizeof(huffCodes));` : membaca data yang telah ditransmisikan dari proses sebelumnya ke dalam array huffCodes melalui pipe yang terhubung dengan fd2[0].

- `input_file = fopen("file.txt", "r");` : membuka file bernama "file.txt" dengan mode membaca ("r") dan menyimpan file descriptor ke dalam variabel input_file.

- `while ((c = fgetc(input_file)) != EOF){` : membaca karakter dari file yang diakses dengan menggunakan fungsi fgetc() dan menyimpannya dalam variabel c hingga mencapai akhir file (EOF).

- `if (isalpha(c)) {` : memeriksa apakah karakter yang dibaca adalah huruf dengan menggunakan fungsi isalpha() dari library ctype.h.

- `sblm += 8;` : menambahkan 8 bit ke variabel sblm yang merepresentasikan jumlah bit sebelum dilakukan kompresi.

- `stlh += strlen(huffCodes[toupper(c) - 'A']);` : menambahkan jumlah bit yang digunakan untuk merepresentasikan huruf tersebut setelah dilakukan kompresi ke variabel stlh dengan menggunakan strlen() untuk menghitung panjang string yang merepresentasikan kode Huffman dari huruf tersebut.

- `fclose(input_file);` : menutup file yang telah dibuka untuk membaca.

### e. 
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

```
printf("Perbandingan :\n");
printf("Jumlah bit sebelum Huffman : %d bits\n", sblm);
printf("JUmlah bit setelah Huffman : %d bits\n", stlh);
```
#### Penjelasan :
- `printf("Jumlah bit sebelum Huffman : %d bits\n", sblm);` dan `printf("JUmlah bit setelah Huffman : %d bits\n", stlh);` : untuk menampilkan jumlah bit sebelum proses kompresi Huffman dan jumlah bit setelah proses kompresi Huffman.

### Huffman Code :
- #### Struct MinHeapNode
```
struct MinHeapNode {
    char data;
    unsigned freq;
    struct MinHeapNode *left, *right;
};
```
`Struct MinHeapNode` berfungsi sebagai representasi node dalam sebuah pohon heap yang menyimpan karakter dan frekuensi kemunculan karakter tersebut.

- #### Struct MinHeap
```
struct MinHeap {
    unsigned size;
    unsigned capacity;
    struct MinHeapNode** array;
};
```
`Struct MinHeap` berfungsi sebagai representasi dari sebuah heap minimum yang menyimpan array dari MinHeapNode.

- #### Struct MinHeapNode* newNode
```
struct MinHeapNode* newNode(char data, unsigned freq)
{
    struct MinHeapNode* temp = (struct MinHeapNode*)malloc(sizeof(struct MinHeapNode));
    temp->left = temp->right = NULL;
    temp->data = data;
    temp->freq = freq;
    return temp;
}
```
`Fungsi newNode` digunakan untuk membuat dan menginisialisasi node baru dalam pohon heap dengan karakter dan frekuensi yang diberikan.

- #### Struct MinHeap* createMinHeap
```
struct MinHeap* createMinHeap(unsigned capacity)
{
    struct MinHeap* minHeap = (struct MinHeap*)malloc(sizeof(struct MinHeap));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = (struct MinHeapNode**)malloc(minHeap->capacity * sizeof(struct MinHeapNode*));
    return minHeap;
}
```
`Fungsi createMinHeap` digunakan untuk membuat dan menginisialisasi heap minimum baru dengan kapasitas yang diberikan.

- #### Void swapMinHeapNode
```
void swapMinHeapNode(struct MinHeapNode** a, struct MinHeapNode** b)
{
    struct MinHeapNode* t = *a;
    *a = *b;
    *b = t;
}
```
`Fungsi swapMinHeapNode` digunakan untuk menukar posisi dua node dalam heap minimum.

- #### Void minHeapify
```
void minHeapify(struct MinHeap* minHeap, int idx)
{
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;
    if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
        smallest = left;
    if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
        smallest = right;
    if (smallest != idx) {
        swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}
```
`Fungsi minHeapify` digunakan untuk memastikan bahwa properti heap minimum terpenuhi pada posisi yang diberikan dalam heap minimum.

- #### Void buildMinHeap
```
void buildMinHeap(struct MinHeap* minHeap)
{
    int n = minHeap->size - 1;
    int i;
    for (i = (n - 1) / 2; i >= 0; --i)
        minHeapify(minHeap, i);
}
```
`Fungsi buildMinHeap` digunakan untuk menginisialisasi heap minimum dengan memanggil minHeapify pada setiap node dalam heap.

- #### Struct MinHeap* createAndBuildMinHeap
```
struct MinHeap* createAndBuildMinHeap(char data[], int freq[], int size)
{
    struct MinHeap* minHeap = createMinHeap(size);
    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(data[i], freq[i]);
    minHeap->size = size;
    buildMinHeap(minHeap);
    return minHeap;
}
```
`Fungsi createAndBuildMinHeap` digunakan untuk membuat dan menginisialisasi heap minimum baru dengan karakter dan frekuensi yang diberikan, serta memanggil buildMinHeap.

- #### Void insertMinHeap
```
void insertMinHeap(struct MinHeap* minHeap, struct MinHeapNode* minHeapNode)
{
    ++minHeap->size;
    int i = minHeap->size - 1;
    while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    minHeap->array[i] = minHeapNode;
}
```
`Fungsi insertMinHeap` digunakan untuk memasukkan node baru ke dalam heap minimum dan memastikan bahwa properti heap minimum terpenuhi.

- #### Struct MinHeapNode* extractMin
```
struct MinHeapNode* extractMin(struct MinHeap* minHeap)
{
    struct MinHeapNode* temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
    --minHeap->size;
    minHeapify(minHeap, 0);
    return temp;
}
```
`Fungsi extractMin` digunakan untuk menghapus dan mengembalikan node dengan frekuensi minimum dari heap minimum, serta memastikan bahwa properti heap minimum terpenuhi setelah penghapusan.

- #### Int isSizeOne
```
int isSizeOne(struct MinHeap* minHeap)
{
    return (minHeap->size == 1);
}
```
`Fungsi isSizeOne` digunakan untuk memeriksa apakah ukuran heap minimum adalah satu.

- #### Struct MinHeapNode* buildHuffmanTree
```
struct MinHeapNode* buildHuffmanTree(char data[], int freq[], int size)
{
    struct MinHeapNode *left, *right, *top;
    struct MinHeap* minHeap = createAndBuildMinHeap(data, freq, size);
    while (!isSizeOne(minHeap)) {
        left = extractMin(minHeap);
        right = extractMin(minHeap);

        top = newNode('$', left->freq + right->freq);

        top->left = left;
        top->right = right;

        insertMinHeap(minHeap, top);
    }
    return extractMin(minHeap);
}
```
`Fungsi buildHuffmanTree` digunakan untuk membangun pohon Huffman dengan menggunakan karakter dan frekuensi yang diberikan.

- #### Int isLeaf
```
int isLeaf(struct MinHeapNode* root)
{
    return !(root->left) && !(root->right);
}
```
`Fungsi isLeaf` digunakan untuk memeriksa apakah suatu node adalah leaf atau bukan.

- #### Void printCodes
```
void printCodes(struct MinHeapNode* root, int arr[], int top, char huffman[][26])
{
    if (root->left) {
        arr[top] = 0;
        printCodes(root->left, arr, top + 1, huffman);
    }
    if (root->right) {
        arr[top] = 1;
        printCodes(root->right, arr, top + 1, huffman);
    }
    if (isLeaf(root)) {
        for (int i = 0; i < top; i++) {
            huffman[root->data - 'A'][i] = arr[i] + '0';
        }
        huffman[root->data - 'A'][top] = '\0';
    }
}
```
`Fungsi printCodes` digunakan untuk mencetak kode Huffman dari setiap karakter dalam pohon Huffman.

- #### Void HuffmanCodes
```
void HuffmanCodes(char huruf[], int freq[], char huffman[][26], int size)
{
    struct MinHeapNode* root = buildHuffmanTree(huruf, freq, size);
    int arr[MAX_TREE_HT], top = 0;
    printCodes(root, arr, top, huffman);
}
```
`Fungsi HuffmanCodes` digunakan untuk membuat pohon Huffman dan mencetak kode Huffman dari setiap karakter yang diberikan.

- #### Void sort
```
void sort(char arr[], int count[], int low, int high) {
    int i, j, temp1;
    char temp2;
    for (i = low; i <= high; i++) {
        for (j = i + 1; j <= high; j++) {
            if (count[j] > count[i]) {
                temp1 = count[i];
                count[i] = count[j];
                count[j] = temp1;
                temp2 = arr[i];
                arr[i] = arr[j];
                arr[j] = temp2;
            }
        }
    }
}
```
`Fungsi sort` digunakan untuk mengurutkan data karakter dan frekuensi kemunculannya dari frekuensi yang paling banyak hingga frekuensi yang paling sedikit. Fungsi ini menerima parameter array karakter, array frekuensi, indeks awal, dan indeks akhir. Fungsi sort menggunakan algoritma bubble sort untuk mengurutkan data.

### Screenshot Program
![](images/Screenshot_from_2023-05-12_10-18-10.png)

## No. 2 
- [kalian.c](https://gitlab.com/tlithaee_/sisop-praktikum-modul-3-2023-bj-a11/-/blob/e9cba567cdf1ac37a5b208f9e62fed0529293b58/soal2/kalian.c)

- [cinta.c](https://gitlab.com/tlithaee_/sisop-praktikum-modul-3-2023-bj-a11/-/blob/e9cba567cdf1ac37a5b208f9e62fed0529293b58/soal2/cinta.c)

- [sisop.c](https://gitlab.com/tlithaee_/sisop-praktikum-modul-3-2023-bj-a11/-/blob/e9cba567cdf1ac37a5b208f9e62fed0529293b58/soal2/sisop.c)

Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

### a.
Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

#### Jawaban :

```
void print_matrix(int row, int col, int matrix[row][col]){
    for(int i=0; i<row; i++){
        for(int j=0; j<col; j++){
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }

```
**Penjelasan :**
1. `void print_matrix(int row, int col, int matrix[row][col])` : ada 3 parameter -> `row` yang merupakan jumlah baris matriks, `col` yang merupakan jumlah kolom matriks, dan `matrix` yang merupakan matriks berukuran row x col
2. kondisi `i<row` dan `j<col` dapat memastikan bahwa semua elemen matriks yang sesuai dengan ukuran matriks yang diberikan sebagai parameter akan tercetak tanpa melampaui batas indeks yang valid

```
// generate random values for matrix a
    srand(time(NULL));
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_A; j++){
            matrix_a[i][j] = (rand() % 5) + 1;
        }
    }

// generate random values for matrix b
    for(int i=0; i<ROW_B; i++){
        for(int j=0; j<COL_B; j++){
            matrix_b[i][j] = (rand() % 4) + 1;
        }
    }

```
**Penjelasan :**
1. `srand(time(NULL));` : untuk menginisialisasi generator bilangan acak dengan sebuah seed. Pada kasus ini, seed yang digunakan adalah `time(NULL)`, yaitu waktu saat ini, sehingga setiap kali program dijalankan, seed akan berbeda dan bilangan acak yang dihasilkan juga akan berbeda.
2. `matrix_a[i][j] = (rand() % 5) + 1;` : Setiap elemen matriks `matrix_a[i][j]` diisi dengan nilai acak antara 1 hingga 5.
3. Fungsi `rand()` : untuk menghasilkan bilangan acak, dan operator `modulo %` digunakan untuk membatasi nilai acak tersebut menjadi rentang 0 hingga 4. Lalu, ditambahkan 1 agar nilainya menjadi antara 1 hingga 5.

```
// multiply matrix a and matrix b
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_B; j++){
            int sum = 0;
            for(int k=0; k<COL_A; k++){
                sum += matrix_a[i][k] * matrix_b[k][j];
            }
            result[i][j] = sum;
        }
    }
```
**Penjelasan :**
1.  `for(int k=0; k<COL_A; k++){ sum += matrix_a[i][k] * matrix_b[k][j];` : untuk melakukan penjumlahan perkalian elemen-elemen matriks a dan b pada setiap posisi yang sesuai. Hasil perkalian tersebut kemudian diakumulasikan dalam variabel sum.
2. `result[i][j] = sum;` : hasil penjumlahan (sum) akan disimpan di dalam matriks hasil (result) pada posisi yang sesuai dengan baris dan kolom yang sedang diakses pada perulangan pertama dan kedua.

```
// create shared memory for result matrix
    key_t key = 1234;
    int shmid = shmget(key, sizeof(result), IPC_CREAT | 0666);
    unsigned long long int (*shm_ptr)[COL_B] = shmat(shmid, NULL, 0);

    // write result matrix to shared memory
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_B; j++){
            shm_ptr[i][j] = result[i][j];
        }
    }
```
**Penjelasan :**

Kode di atas digunakan untuk menyalin matriks hasil ke memori bersama, yang nantinya dapat diakses oleh proses lain yang menggunakan kunci yang sama.

```
sleep(10);

    // detach and delete shared memory
    shmdt(shm_ptr);
    shmctl(shmid, IPC_RMID, NULL);
```

**Penjelasan :**
1. `sleep(10);` : untuk memberikan penundaan eksekusi program selama 10 detik
2. `shmdt(shm_ptr);` : untuk melepaskan memori bersama yang telah dilampirkan ke proses saat ini dengan `shm_ptr` sebagai pointer ke memori tersebut. 
3. `shmctl(shmid, IPC_RMID, NULL);` : untuk mengendalikan memori bersama. `IPC_RMID` memberi tahu sistem operasi bahwa segmen memori bersama dengan ID shmid harus dihapus setelah tidak digunakan lagi.

### Output

![](images/kalian.c.png)

### b. 
Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)

#### Jawaban :
```
// struct to pass arguments to thread function
struct arg_struct {
    int row;
    int col;
    unsigned long long int val;
};
```

**Penjelasan :**
1. `arg_struct` : digunakan untuk melewatkan argumen ke dalam fungsi thread.
2. `int row` : untuk menyimpan nilai baris.
3. `int col` : untuk menyimpan nilai kolom.
4. `unsigned long long int val` : untuk menyimpan nilai.

### c.

Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 
array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

maka:

1 2 6 24 120 720 ... ... …
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)

#### Jawaban :


```
// function to calculate factorial
void *factorial(void *arguments){
    struct arg_struct *args = arguments;
    unsigned long long int val = args->val;
    unsigned long long int res[200] = {0};
    res[0] = 1;
    int len = 1;
    for(int i=2; i<=val; i++){
        int carry = 0;
        for(int j=0; j<len; j++){
            int mul = res[j] * i + carry;
            res[j] = mul % 10;
            carry = mul / 10;
        }
        while(carry){
            res[len] = carry % 10;
            carry /= 10;
            len++;
        }
    }
    char *result = (char*)malloc((len + 1) * sizeof(char));
    for(int i=len-1, j=0; i>=0; i--, j++){
        result[j] = res[i] + '0';
    }
    result[len] = '\0';
    return (void*)result;
}
```
**Penjelasan :**
1. Fungsi factorial menerima argumen berupa pointer ke struktur `arg_struct`, yang berisi nilai bilangan yang akan dihitung faktorialnya.
2. `val` : untuk menyimpan nilai bilangan dari struktur argumen.
3. `Array res` dengan ukuran 200 digunakan untuk menyimpan hasil faktorial dalam bentuk array digit.
4. Di dalam loop for, setiap digit hasil faktorial dihitung dan disimpan dalam array res menggunakan operasi matematika.
Setelah selesai perhitungan, array res akan berisi digit-digit hasil faktorial dalam urutan terbalik.
5. `Array res` kemudian diubah menjadi string dengan menggunakan alokasi memori dinamis untuk string result.
6. Digit-digit dalam `array res` diubah menjadi karakter angka dengan menambahkan karakter '0', dan kemudian disimpan dalam string result.
7. String result diakhiri dengan karakter null-terminator `'\0'`.

```
// get shared memory for result matrix
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int[ROW_A][COL_B]), 0666);
    unsigned long long int (*shm_ptr)[COL_B] = shmat(shmid, NULL, 0);

    // print result matrix from shared memory
    printf("Result:\n");
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_B; j++){
            printf("%llu ", shm_ptr[i][j]);
        }
        printf("\n");
    }

```
**Penjelasan :**

1. Mendefinisikan `key` untuk mengakses memori bersama dengan nilai 1234.
2. Menggunakan fungsi `shmget()` untuk mendapatkan id memori bersama dengan ukuran yang sesuai dengan matriks hasil `int[ROW_A][COL_B]`. Mode `0666` menentukan izin akses ke memori bersama.
3. Menggunakan fungsi `shmat()` untuk menghubungkan memori bersama yang diperoleh sebelumnya `shmid` ke dalam ruang alamat proses saat ini.`shm_ptr` adalah pointer ke matriks hasil.

```
    // detach and delete shared memory
    shmdt(shm_ptr);
    shmctl(shmid, IPC_RMID, NULL);
```

**Penjelasan :**

1. `shmdt(shm_ptr);` : untuk melepaskan memori bersama yang telah dilampirkan ke proses saat ini dengan `shm_ptr` sebagai pointer ke memori tersebut. 
2. `shmctl(shmid, IPC_RMID, NULL);` : untuk mengendalikan memori bersama. `IPC_RMID` memberi tahu sistem operasi bahwa segmen memori bersama dengan ID shmid harus dihapus setelah tidak digunakan lagi.

### Output

![](images/cinta.c.png)

### d.
Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 

#### Jawaban :

```
// function to calculate factorial
void factorial(int n, char *result){
    int res[200] = {0};
    res[0] = 1;
    int len = 1;
    for(int i=2; i<=n; i++){
        int carry = 0;
        for(int j=0; j<len; j++){
            int mul = res[j] * i + carry;
            res[j] = mul % 10;
            carry = mul / 10;
        }
        while(carry){
            res[len] = carry % 10;
            carry /= 10;
            len++;
        }
    }
    for(int i=len-1, j=0; i>=0; i--, j++){
        result[j] = res[i] + '0';
    }
}
```
**Penjelasan :**

1. Menginisialisasi elemen pertama dari `res` dengan nilai 1, karena faktorial dari 0 dan 1 adalah 1.
2. `len` untuk melacak panjang hasil faktorial dalam array res, awalnya diatur sebagai 1 karena hanya ada satu digit pada awalnya.
3. `carry` untuk menyimpan sisa pembagian setelah melakukan perkalian antara digit faktorial sebelumnya dengan faktor i.
4. Melakukan perulangan dari 0 hingga len-1 untuk mengalikan setiap digit faktorial sebelumnya dengan faktor i dan mengupdate array res.
5. Setelah perulangan selesai, `array res` akan berisi hasil faktorial dengan urutan digit terbalik, di mana digit terakhir berada pada indeks len-1.
6. Perulangan terakhir digunakan untuk mengonversi hasil faktorial dalam array res menjadi karakter dan menyimpannya dalam array result secara terbalik. Pada setiap iterasi, elemen `res[i]` diubah menjadi karakter dengan menambahkannya dengan nilai `ASCII` untuk `karakter '0'` (konversi dari angka ke karakter).
7. Setelah selesai, array result akan berisi string yang merupakan hasil faktorial dari bilangan n yang diinputkan

```
// get shared memory for result matrix
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int[ROW_A][COL_B]), 0666);
    unsigned long long int (*shm_ptr)[COL_B] = shmat(shmid, NULL, 0);

    // print result matrix from shared memory
    printf("Result:\n");
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_B; j++){
            printf("%llu ", shm_ptr[i][j]);
        }
        printf("\n");
    }

```
**Penjelasan :**

Kode di atas digunakan untuk menyalin matriks hasil ke memori bersama, yang nantinya dapat diakses oleh proses lain yang menggunakan kunci yang sama.

```
// calculate factorial of each element in matrix
    printf("\nFactorial:\n");
    char result[200];
    for(int i=0; i<ROW_A; i++){
        for(int j=0; j<COL_B; j++){
            factorial(shm_ptr[i][j], result);
            printf("%s ", result);
        }
        printf("\n");
    }

```
**Penjelasan :**

1. `for(int i=0; i<ROW_A; i++)` : Memulai loop untuk mengiterasi setiap baris dalam matriks. Variabel "i" digunakan sebagai indeks baris.
2. `for(int j=0; j<COL_B; j++)` : Memulai loop untuk mengiterasi setiap kolom dalam matriks. Variabel "j" digunakan sebagai indeks kolom.
3. `factorial(shm_ptr[i][j], result);` : Memanggil fungsi "factorial" dengan argumen elemen matriks yang berada pada baris "i" dan kolom "j". Fungsi "factorial" akan menghitung faktorial dari elemen tersebut dan hasilnya akan disimpan di dalam array "result".

```
    // detach and delete shared memory
    shmdt(shm_ptr);
    shmctl(shmid, IPC_RMID, NULL);
```

**Penjelasan :**

1. `shmdt(shm_ptr);` : untuk melepaskan memori bersama yang telah dilampirkan ke proses saat ini dengan `shm_ptr` sebagai pointer ke memori tersebut. 
2. `shmctl(shmid, IPC_RMID, NULL);` : untuk mengendalikan memori bersama. `IPC_RMID` memberi tahu sistem operasi bahwa segmen memori bersama dengan ID shmid harus dihapus setelah tidak digunakan lagi.

### Output

![](images/sisop.c.png)

## No. 3 
- [stream.c](https://gitlab.com/tlithaee_/sisop-praktikum-modul-3-2023-bj-a11/-/blob/e9cba567cdf1ac37a5b208f9e62fed0529293b58/soal3/stream.c)

- [user.c](https://gitlab.com/tlithaee_/sisop-praktikum-modul-3-2023-bj-a11/-/blob/e9cba567cdf1ac37a5b208f9e62fed0529293b58/soal3/user.c)

Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

### a.
Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

#### Jawaban :


```
struct message {
  long message_type;
  char message_text[MAX_SIZE];
  int user_pid; // menambahkan variabel sender_id
};

int main() {
  key_t key = ftok("stream.c", 'a');
  int msgid = msgget(key, 0666 | IPC_CREAT);
  if (msgid == -1) {
    printf("Error: cannot create message queue\n");
    exit(1);
  }
  struct message message;


```
**Penjelasan :**

1. `struct message` : Mendefinisikan sebuah struktur dengan nama message. Struktur ini memiliki tiga anggota yaitu `message_type (tipe pesan)`, `message_text (teks pesan)`, dan `user_pid (ID pengirim).
2. `key_t key = ftok("stream.c", 'a');` : Membuat sebuah kunci unik dengan menggunakan fungsi `ftok()`. Fungsi ini mengambil argumen berupa path ke file (stream.c) dan karakter ('a') untuk menghasilkan kunci yang unik. Kunci ini digunakan untuk mengidentifikasi antrian pesan.
3. `int msgid = msgget(key, 0666 | IPC_CREAT);` : Membuat atau mengakses antrian pesan dengan menggunakan fungsi msgget(). Fungsi ini mengambil argumen berupa kunci (key) dan mode akses (0666 | IPC_CREAT). Jika antrian pesan dengan kunci tersebut sudah ada, maka akan mengembalikan ID antrian pesan yang terkait. Jika antrian pesan belum ada, maka akan membuat antrian pesan baru dan mengembalikan ID antrian pesan tersebut. Mode akses 0666 | IPC_CREAT digunakan untuk memberikan hak akses pada antrian pesan yang dapat dibaca dan ditulis oleh semua pengguna.
4. `if (msgid == -1)` : Memeriksa apakah terjadi error dalam pembuatan antrian pesan. Jika msgid bernilai -1, artinya terjadi error dalam pembuatan antrian pesan.
5. `struct message message;` : Membuat variabel dengan nama message bertipe struct message. Variabel ini digunakan untuk menyimpan data pesan yang akan dikirim atau diterima melalui antrian pesan.

```
char input[MAX_SIZE];
    int quit = 0;
    while (!quit) {
        printf("Enter message (\"quit\" to exit): ");
        fgets(input, MAX_SIZE, stdin);
        input[strcspn(input, "\n")] = '\0'; 
        if (strncmp(input, "ADD ", 3) == 0 || strncmp(input, "DECRYPT", 7) == 0 || strncmp(input, "LIST", 4) == 0 || strncmp(input, "PLAY ", 4) == 0 || strcmp(input, "quit") == 0|| strcmp(input, "QUIT") == 0) {
                pid_t user_pid = getpid();
                message.user_pid = user_pid;
                strncpy(message.message_text, input, MAX_SIZE);

            } else {
                message.user_pid = 0; 
                strcpy(message.message_text, input);
            }

```
**Penjelasan :**
1. `char input[MAX_SIZE];` : Mendeklarasikan sebuah array karakter bernama input dengan ukuran maksimum MAX_SIZE. Array ini akan digunakan untuk menyimpan input pesan dari pengguna.
2. `int quit = 0;` : Mendeklarasikan variabel integer quit dengan nilai awal 0. Variabel ini akan digunakan sebagai kondisi untuk keluar dari loop.
3. `while (!quit)` : Memulai loop while yang akan terus berjalan selama quit bernilai 0, atau dalam kata lain, selama quit tidak bernilai true.
4. `fgets(input, MAX_SIZE, stdin);` : Membaca input dari pengguna menggunakan fungsi fgets. Input tersebut akan disimpan dalam array input dengan batasan maksimum MAX_SIZE.
5. `input[strcspn(input, "\n")] = '\0';` : Menghapus karakter newline ('\n') yang ada di dalam string input. Fungsi strcspn digunakan untuk mencari posisi karakter newline ('\n') dalam string input, kemudian karakter tersebut digantikan dengan karakter null ('\0').
6. `if (strncmp(input, "ADD ", 3) == 0 || strncmp(input, "DECRYPT", 7) == 0 || strncmp(input, "LIST", 4) == 0 || strncmp(input, "PLAY ", 5) == 0 || strcmp(input, "quit") == 0 || strcmp(input, "QUIT") == 0) ` : Memeriksa apakah input pengguna sesuai dengan salah satu opsi yang diberikan. Opsi yang valid adalah jika input diawali dengan "ADD ", "DECRYPT", "LIST", "PLAY ", "quit", atau "QUIT". Jika input sesuai dengan salah satu opsi ini, maka blok kode dalam if statement akan dieksekusi.
7. `pid_t user_pid = getpid();` : Mendeklarasikan variabel user_pid dengan tipe pid_t untuk menyimpan ID proses pengguna. Fungsi getpid() digunakan untuk mendapatkan ID proses pengguna saat ini.
8. `message.user_pid = user_pid;` : Menetapkan nilai user_pid ke dalam variabel message.user_pid. Ini mengisi field user_pid dalam objek message dengan ID proses pengguna.
9. `strncpy(message.message_text, input, MAX_SIZE);` : Meng-copy string input ke dalam field message.message_text dalam objek message. Fungsi strncpy digunakan untuk meng-copy sebagian atau seluruh string dari input ke message.message_text dengan panjang maksimum MAX_SIZE.
10. `message.user_pid = 0;` : Mengatur nilai user_pid dalam objek message menjadi 0,

### Output

![](images/manggilfungsi.png)

### b.
User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket sehingga struktur direktorinya adalah sebagai berikut:

└── soal3
	├── playlist.txt
	├── song-playlist.json
	├── stream.c
	└── user.c

#### Jawaban :

```
// Fungsi untuk mendecrypt dengan rot13
char* rot13_decode(const char* str) {
    int i = 0;
    char* output = strdup(str);
    while (output[i] != '\0') {
        if (isalpha(output[i])) {
            if ((output[i] >= 'a' && output[i] <= 'm') || (output[i] >= 'A' && output[i] <= 'M')) {
                output[i] += 13;
            } else if ((output[i] >= 'n' && output[i] <= 'z') || (output[i] >= 'N' && output[i] <= 'Z')) {
                output[i] -= 13;
            }
        }
        i++;
    }
    return output;
}

```
**Penjelasan :**

1. `char* rot13_decode(const char* str)` : deklarasi fungsi rot13_decode yang mengambil parameter str dengan tipe data const char* (pointer ke karakter konstan) dan mengembalikan hasil dalam bentuk char* (pointer ke karakter).
2. `int i = 0;` : Mendeklarasikan dan menginisialisasi variabel i dengan nilai 0. Variabel ini akan digunakan sebagai indeks saat melakukan iterasi melalui string.
3. `char* output = strdup(str);` : Membuat salinan string str dengan menggunakan fungsi strdup. String hasil salinan disimpan dalam variabel output. Ini dilakukan agar string asli str tidak dimodifikasi saat proses dekripsi.
4. `while (output[i] != '\0')` : Memulai loop while yang akan berjalan selama karakter saat ini dalam output bukan karakter null ('\0'). Loop ini akan melanjutkan pengolahan hingga akhir string.
5. `if (isalpha(output[i]))` : Memeriksa apakah karakter saat ini dalam output adalah karakter alfabet menggunakan fungsi isalpha. Jika benar, blok kondisional ini akan dieksekusi.
6. `if ((output[i] >= 'a' && output[i] <= 'm') || (output[i] >= 'A' && output[i] <= 'M'))` : Memeriksa apakah karakter saat ini adalah huruf kecil antara 'a' dan 'm' atau huruf besar antara 'A' dan 'M'. Jika benar, karakter tersebut akan diubah dengan menambahkan 13 ke nilai ASCII-nya. Ini adalah langkah dekripsi ROT13 untuk huruf-huruf dalam setengah pertama abjad.
7. `output[i] += 13;` : Menambahkan 13 ke nilai ASCII dari karakter saat ini dalam output. Ini akan mengubah karakter tersebut menjadi karakter setelah 13 langkah dalam urutan ASCII.
8. `else if ((output[i] >= 'n' && output[i] <= 'z') || (output[i] >= 'N' && output[i] <= 'Z'))` : Memeriksa apakah karakter saat ini adalah huruf kecil antara 'n' dan 'z' atau huruf besar antara 'N' dan 'Z'. Jika benar, karakter tersebut akan diubah dengan mengurangi 13 dari nilai ASCII-nya. Ini adalah langkah dekripsi ROT13 untuk huruf-huruf dalam setengah kedua abjad.
9. `output[i] -= 13;` : Mengurangi 13 dari nilai ASCII karakter saat ini dalam output. Ini akan mengubah karakter tersebut menjadi karakter sebelum 13 langkah dalam urutan ASCII.
10. `return output;` : Mengembalikan pointer output yang berisi string hasil dekripsi ROT13.

```
// Fungsi untuk mendecrypt dengan base64
char* base64_decode(const char* input) {
    size_t input_length = strlen(input);
    size_t output_length = (input_length / 4) * 3;
    if (input[input_length - 1] == '=') {
        output_length--;
    }
    if (input[input_length - 2] == '=') {
        output_length--;
    }

    char* output = (char*)malloc(output_length + 1);
    if (output == NULL) {
        return NULL;
    }

    const char* base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    int j = 0;
    for (int i = 0; i < input_length; i += 4) {
        int a = strchr(base64_chars, input[i]) - base64_chars;
        int b = strchr(base64_chars, input[i + 1]) - base64_chars;
        int c = strchr(base64_chars, input[i + 2]) - base64_chars;
        int d = strchr(base64_chars, input[i + 3]) - base64_chars;

        uint32_t buffer = (a << 18) | (b << 12) | (c << 6) | d;

        if (input[i + 2] == '=') {
            output[j++] = (buffer >> 16) & 0xff;
        } else if (input[i + 3] == '=') {
            output[j++] = (buffer >> 16) & 0xff;
            output[j++] = (buffer >> 8) & 0xff;
        } else {
            output[j++] = (buffer >> 16) & 0xff;
            output[j++] = (buffer >> 8) & 0xff;
            output[j++] = buffer & 0xff;
        }
    }

    output[output_length] = '\0';
    return output;
}
```
**Penjelasan :**

1. `char* base64_decode(const char* input)` : Ini adalah deklarasi fungsi base64_decode yang menerima argumen input berupa pointer ke karakter yang akan didekripsi.
2. `size_t input_length = strlen(input);` : Mencari panjang string input dengan menggunakan fungsi strlen. Panjang string ini akan digunakan untuk melakukan iterasi dalam proses dekripsi.
3. `size_t output_length = (input_length / 4) * 3;` : Menghitung panjang teks hasil dekripsi. Setiap 4 karakter dalam encoding Base64 akan menghasilkan 3 karakter dalam teks asli. Sehingga panjang teks asli bisa dihitung dengan rumus (input_length / 4) * 3.
4. `char* output = (char*)malloc(output_length + 1);` : Mengalokasikan memori untuk menyimpan teks hasil dekripsi. Panjang memori yang dialokasikan adalah output_length + 1 untuk menyertakan karakter null-terminator ('\0') pada akhir teks.
5. `const char* base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";` : String base64_chars berisi karakter-karakter yang digunakan dalam encoding Base64. Karakter ini akan digunakan untuk mencari nilai numerik dari karakter yang akan didekripsi
6. `output[output_length] = '\0';` : Mengakhiri string output dengan karakter null-terminator ('\0') untuk menandakan akhir dari string.
7. `return output;` : Mengembalikan pointer ke string `output

```
// Fungsi untuk mendecrypt dengan hex
char* hex_decode(const char* str) {
    int len = strlen(str);
    char* output = (char*)calloc((len / 2) + 1, sizeof(char));
    char byte_str[3] = {'\0', '\0', '\0'};
    int j = 0;

    for (int i = 0; i < len; i += 2) {
        byte_str[0] = str[i];
        byte_str[1] = str[i + 1];
        output[j++] = strtol(byte_str, NULL, 16);
    }

    return output;
}
```
**Penjelasan :**
1. `char* hex_decode(const char* str)` : Mendefinisikan fungsi hex_decode yang menerima sebuah string (const char* str) dan mengembalikan pointer ke karakter (char*).
2. `int len = strlen(str)` : Menghitung panjang string str menggunakan fungsi strlen() dan menyimpan nilainya dalam variabel len.
3. `char* output = (char*)calloc((len / 2) + 1, sizeof(char))` : Mengalokasikan memori untuk string output dengan ukuran yang cukup untuk menyimpan hasil dekripsi. Ukuran alokasi dihitung dengan membagi panjang string input (len) dengan 2 dan menambahkan 1 untuk karakter null-terminator. Fungsi calloc() digunakan untuk menginisialisasi semua elemen dengan nilai nol.
4. `char byte_str[3] = {'\0', '\0', '\0'}` : Mendeklarasikan array byte_str yang berukuran 3 dan diinisialisasi dengan karakter null-terminator.
5. `byte_str[0] = str[i] dan byte_str[1] = str[i + 1]` : Mengisi elemen pertama dan kedua dari array byte_str dengan karakter heksadesimal dari string input, yaitu str[i] dan str[i + 1].
6. `output[j++] = strtol(byte_str, NULL, 16)` : Mengkonversi string heksadesimal byte_str menjadi bilangan bulat menggunakan fungsi strtol(), dengan basis 16. Nilai yang dihasilkan kemudian disimpan di dalam elemen ke-j dari string output (output[j]), dan nilai j ditambahkan setelahnya.

```
int compare_strings(const void* a, const void* b) {
    const char** string_a = (const char**) a;
    const char** string_b = (const char**) b;
 
    return strcasecmp(*string_a, *string_b);
}

```
**Penjelasan :**

1. `int compare_strings(const void* a, const void* b)` : Ini adalah deklarasi fungsi compare_strings yang mengambil dua argumen bertipe const void* dan mengembalikan nilai bertipe int. Argumen ini bertipe const void* karena fungsi qsort dapat menerima elemen-elemen dengan tipe data yang berbeda. Dalam kasus ini, elemen-elemen yang dibandingkan adalah dua pointer ke string (const char**).
2. `const char** string_a = (const char**) a;` : Baris ini menginisialisasi variabel string_a sebagai pointer ke pointer char (const char**) dan mengkonversi argumen a yang bertipe const void* menjadi const char**. Hal ini dilakukan karena qsort mengharuskan argumen perbandingan untuk memiliki tipe const void*, sehingga perlu dilakukan pengecoran (casting) tipe untuk mengakses nilai yang sesuai.
3. `const char** string_b = (const char**) b;` : Baris ini menginisialisasi variabel string_b sebagai pointer ke pointer char (const char**) dan mengkonversi argumen b yang bertipe const void* menjadi const char**.
4. `return strcasecmp(*string_a, *string_b);` : membandingkan dua string yang ditunjuk oleh string_a dan string_b menggunakan fungsi strcasecmp. Fungsi strcasecmp merupakan fungsi bawaan dalam C yang membandingkan dua string secara case-insensitive. Jika string pertama (yang ditunjuk oleh string_a) kurang dari string kedua (yang ditunjuk oleh string_b), fungsi ini akan mengembalikan nilai negatif. Jika string pertama lebih besar, maka fungsi ini akan mengembalikan nilai positif. Jika kedua string sama, fungsi ini akan mengembalikan nilai 0.

```
void decrypt(const char *input_file, const char *output_file) {
    FILE *fp;
    FILE *out_fp;
    char buffer[100000];

    struct json_object *parsed_json;
    struct json_object *method;
    struct json_object *song;

    size_t n_objects;
    size_t i;

    fp = fopen(input_file, "r");
    fread(buffer, 100000, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);

    if (parsed_json == NULL) {
        fprintf(stderr, "Error: failed to parse JSON\n");
        return;
    }

    out_fp = fopen(output_file, "w");

    if (json_object_is_type(parsed_json, json_type_array)) {
        n_objects = json_object_array_length(parsed_json);

        char **decrypted_songs = (char **) malloc(n_objects * sizeof(char *));

        for (i = 0; i < n_objects; i++) {
            struct json_object *obj = json_object_array_get_idx(parsed_json, i);

            json_object_object_get_ex(obj, "method", &method);
            json_object_object_get_ex(obj, "song", &song);

            char *decrypted_song;
            if (strcmp(json_object_get_string(method), "rot13") == 0) {
                decrypted_song = rot13_decode(strdup(json_object_get_string(song)));
            } else if (strcmp(json_object_get_string(method), "base64") == 0) {
                decrypted_song = base64_decode(strdup(json_object_get_string(song)));
            } else if (strcmp(json_object_get_string(method), "hex") == 0) {
                decrypted_song = hex_decode(strdup(json_object_get_string(song)));
            } else {
                decrypted_song = strdup(json_object_get_string(song));
            }

            decrypted_songs[i] = decrypted_song;
        }

        qsort(decrypted_songs, n_objects, sizeof(char *), compare_strings);

        for (i = 0; i < n_objects; i++) {
            fprintf(out_fp, "%s\n", decrypted_songs[i]);
            free(decrypted_songs[i]);
        }

        free(decrypted_songs);
    }

    fclose(out_fp);
}
```
**Penjelasan :**

1. `void decrypt(const char *input_file, const char *output_file)` : Membuka definisi dari fungsi decrypt dengan dua parameter yaitu input_file dan output_file. Fungsi ini tidak mengembalikan nilai (void).
2. `FILE *fp;` : Mendeklarasikan pointer fp yang akan digunakan untuk menunjuk ke file input.
3. `FILE *out_fp;` : Mendeklarasikan pointer out_fp yang akan digunakan untuk menunjuk ke file output.
4. `char buffer[100000];` : Mendeklarasikan array karakter buffer dengan ukuran 100000 yang akan digunakan untuk menyimpan isi file input.
5. `struct json_object *parsed_json;` : Mendeklarasikan pointer parsed_json yang akan digunakan untuk menyimpan hasil parsing JSON.
6. `struct json_object *method;` : Mendeklarasikan pointer method yang akan digunakan untuk menyimpan objek JSON "method".
7. `struct json_object *song;` : Mendeklarasikan pointer song yang akan digunakan untuk menyimpan objek JSON "song".
8. `size_t n_objects;` : Mendeklarasikan variabel n_objects bertipe size_t yang akan menyimpan jumlah objek dalam JSON.
9. `size_t i;` : Mendeklarasikan variabel i bertipe size_t yang akan digunakan sebagai iterator dalam loop.
10. `fp = fopen(input_file, "r");` : Membuka file input dengan nama yang diberikan dalam mode "r" (read) dan menetapkan pointer fp ke file tersebut.
11. `fread(buffer, 100000, 1, fp);` : Membaca isi file input sebanyak 100000 byte dan menyimpannya dalam buffer.
12. `fclose(fp);` : Menutup file input yang sudah dibaca.
13. `parsed_json = json_tokener_parse(buffer);` : Parsing buffer yang berisi JSON dan menyimpan hasil parsing dalam parsed_json.
14. `if (parsed_json == NULL)` : Memeriksa apakah parsing JSON berhasil atau tidak.
15. `fprintf(stderr, "Error: failed to parse JSON\n");` : Jika parsing gagal, maka mencetak pesan kesalahan ke stderr.
16. `out_fp = fopen(output_file, "w");` : Membuka file output dengan nama yang diberikan dalam mode "w" (write) dan menetapkan pointer out_fp ke file tersebut.
17. `if (json_object_is_type(parsed_json, json_type_array))` : Memeriksa apakah parsed_json merupakan array JSON.
18. `n_objects = json_object_array_length(parsed_json);` : Mengambil jumlah objek dalam array JSON dan menyimpannya dalam n_objects.
19. `char **decrypted_songs = (char **) malloc(n_objects * sizeof(char *));` : Mendeklarasikan sebuah array of pointer to char 
20. Memulai loop for dari `i = 0` hingga `i < n_objects`.
21. Memperoleh objek JSON ke-i dari array dengan menggunakan fungsi `json_object_array_get_idx()`.
22. Memperoleh nilai `"method"` dan `"song"` dari objek menggunakan fungsi json_object_object_get_ex().
23. Membuat pointer to char `decrypted_song`.
Jika nilai dari "method" adalah "rot13", maka lakukan dekripsi ROT13 terhadap nilai dari "song" menggunakan fungsi rot13_decode() dan alokasi memori baru dengan menggunakan fungsi strdup(). Assign hasil dekripsi ke dalam decrypted_song.
24. Jika nilai dari "method" adalah "base64", maka lakukan dekripsi Base64 terhadap nilai dari "song" menggunakan fungsi base64_decode() dan alokasi memori baru dengan menggunakan fungsi strdup(). Assign hasil dekripsi ke dalam decrypted_song.
25. Jika nilai dari "method" adalah "hex", maka lakukan dekripsi Hex terhadap nilai dari "song" menggunakan fungsi hex_decode() dan alokasi memori baru dengan menggunakan fungsi strdup(). Assign hasil dekripsi ke dalam decrypted_song.
26. Jika nilai dari "method" bukan "rot13", "base64", ataupun "hex", maka assign nilai "song" ke dalam decrypted_song.
27. Assign decrypted_song ke dalam decrypted_songs[i].
Melakukan sorting pada array decrypted_songs menggunakan fungsi qsort() dengan menggunakan fungsi compare_strings sebagai fungsi pembanding.
28. Memulai loop for dari i = 0 hingga i < n_objects.
Menuliskan nilai decrypted_songs[i] ke dalam file output menggunakan fungsi fprintf().
29. Membuang memori yang telah dialokasikan untuk decrypted_songs[i] menggunakan fungsi free().
30. Membuang memori yang telah dialokasikan untuk decrypted_songs menggunakan fungsi free().
Menutup file output yang telah dibuka dengan menggunakan fungsi `fclose()`.

### Output

![](images/playlist.png)

### c. 
Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt
Sample Output:

- 17 - MK
- 1-800-273-8255 - Logic
- 1950 - King Princess
- …
- Your Love Is My Drug - Kesha
- YOUTH - Troye Sivan
- ZEZE (feat. Travis Scott & Offset) - Kodak Black

#### Jawaban :


```
void print_playlist() {
    FILE* playlist_fp = fopen("playlist.txt", "r");
    if (playlist_fp == NULL) {
        perror("Error: failed to open playlist file");
        return;
    }

    char line[MAX_SIZE];
    while (fgets(line, sizeof(line), playlist_fp)) {
        printf("%s", line);
    }

    fclose(playlist_fp);
}
```
**Penjelasan :**

1. void print_playlist()` : Membuka blok kode untuk fungsi print_playlist yang tidak mengembalikan nilai (void).
2. `FILE* playlist_fp = fopen("playlist.txt", "r");` : Membuka file "playlist.txt" dengan mode "r" (read) dan mengassign file pointer ke variabel playlist_fp. Jika file tidak dapat dibuka, maka akan tercetak pesan error menggunakan perror dan fungsi akan langsung kembali (return).
3. `f (playlist_fp == NULL)` : Memeriksa apakah file pointer playlist_fp bernilai NULL, yang menandakan bahwa file tidak berhasil dibuka.
4. `perror("Error: failed to open playlist file");` : Mencetak pesan error bahwa gagal membuka file playlist.
5. `char line[MAX_SIZE];` : Mendeklarasikan sebuah array karakter bernama line dengan ukuran MAX_SIZE. MAX_SIZE seharusnya telah didefinisikan sebelumnya dalam kode.
6. `while (fgets(line, sizeof(line), playlist_fp))` : Membaca setiap baris dari file menggunakan fgets, dan menyimpannya ke dalam variabel line. Jika masih ada baris yang bisa dibaca (tidak mencapai akhir file), maka loop akan berlanjut.
7. `fclose(playlist_fp);` : Menutup file yang telah selesai dibaca.

### Output

![](images/list.png)

### d. 
User juga dapat mengirimkan perintah PLAY <SONG> dengan ketentuan sebagai berikut.

- PLAY "Stereo Heart" 
    sistem akan menampilkan: 
    USER <USER_ID> PLAYING "GYM CLASS HEROES - STEREO HEART"
PLAY "BREAK"
    sistem akan menampilkan:
    THERE ARE "N" SONG CONTAINING "BREAK":
    1. THE SCRIPT - BREAKEVEN
    2. ARIANA GRANDE - BREAK FREE
dengan “N” merupakan banyaknya lagu yang sesuai dengan string query. Untuk contoh di atas berarti THERE ARE "2" SONG CONTAINING "BREAK":
PLAY "UVUWEVWEVWVE"
    THERE IS NO SONG CONTAINING "UVUVWEVWEVWE"

```
void play(char *song_name, pid_t user_pid) {
    char *start_quote = strchr(song_name, '"');
    if (start_quote == NULL) {
        printf("Error: invalid song name format\n");
        return;
    }
    char *end_quote = strchr(start_quote + 1, '"');
    if (end_quote == NULL) {
        printf("Error: invalid song name format\n");
        return;
    }
    strncpy(song_name, start_quote + 1, end_quote - start_quote - 1);
    song_name[end_quote - start_quote - 1] = '\0';

    FILE *playlist_fp = fopen("playlist.txt", "r");
    if (playlist_fp == NULL) {
        perror("Error: failed to open playlist file");
        return;
    }

    char line[MAX_SIZE];
    int n_songs = 0;

    while (fgets(line, sizeof(line), playlist_fp)) {
        if (strcasestr(line, song_name)) {
            n_songs++;
        }
    }

    fclose(playlist_fp);

    if (n_songs == 0) {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", song_name);
    } else if (n_songs == 1) {
        playlist_fp = fopen("playlist.txt", "r");
        if (playlist_fp == NULL) {
            perror("Error: failed to open playlist file");
            return;
        }
        while (fgets(line, sizeof(line), playlist_fp)) {
            if (strcasestr(line, song_name)) {
            printf("USER %d PLAYING %s", (int)user_pid, line);
                break;
            }
        }
        fclose(playlist_fp);
    } else {
        playlist_fp = fopen("playlist.txt", "r");
        if (playlist_fp == NULL) {
            perror("Error: failed to open playlist file");
            return;
        }
        int song_number = 1;
        while (fgets(line, sizeof(line), playlist_fp)) {
            if (strcasestr(line, song_name)) {
                printf("%d. %s", song_number, line);
                song_number++;
            }
        }
        printf("THERE ARE \"%d\" SONG CONTAINING \"%s\"\n", n_songs, song_name);
        fclose(playlist_fp);
    }
}
```
**Penjelasan :**

1. `char *start_quote = strchr(song_name, '"');` : Mencari posisi pertama tanda kutip ("") pada song_name dan menyimpannya pada start_quote.
2. `if (start_quote == NULL)` : Jika start_quote tidak ditemukan, maka program akan mencetak pesan error dan keluar dari fungsi.
3. `char *end_quote = strchr(start_quote + 1, '"');` : Mencari posisi kedua tanda kutip pada song_name setelah posisi pertama dan menyimpannya pada end_quote.
4. `if (end_quote == NULL)` : Jika end_quote tidak ditemukan, maka program akan mencetak pesan error dan keluar dari fungsi.
5. `strncpy(song_name, start_quote + 1, end_quote - start_quote - 1);` : Menyalin nama lagu dari song_name mulai dari posisi setelah tanda kutip pertama hingga sebelum tanda kutip kedua, dan menyimpannya kembali pada song_name.
6. `song_name[end_quote - start_quote - 1] = '\0';` : Menambahkan karakter null pada akhir nama lagu yang telah disalin.
7. `FILE *playlist_fp = fopen("playlist.txt", "r");` : Membuka file "playlist.txt" dalam mode baca ("r") dan menyimpannya pada variabel playlist_fp.
8. `if (playlist_fp == NULL)` : Jika file tidak dapat dibuka, maka program akan mencetak pesan error dan keluar dari fungsi.
9. `char line[MAX_SIZE];` : Membuat array line dengan ukuran MAX_SIZE untuk menampung setiap baris pada file playlist.
10. `int n_songs = 0;` : Menyimpan jumlah lagu dengan nama yang sama dengan yang diminta pada variabel n_songs.
11. `while (fgets(line, sizeof(line), playlist_fp))` : Membaca setiap baris pada file playlist dan menyimpannya pada line.
12. `if (strcasestr(line, song_name))` : Jika nama lagu yang diminta terdapat pada line, maka n_songs akan bertambah 1.
13. `fclose(playlist_fp);` : Menutup file "playlist.txt".
14. `playlist_fp = fopen("playlist.txt", "r");` : membuka file playlist.txt dalam mode baca.
15. `while (fgets(line, sizeof(line), playlist_fp))` : membaca baris pada file playlist.txt.
16. `if (strcasestr(line, song_name))` : memeriksa apakah baris yang sedang dibaca mengandung nama lagu yang dimasukkan pengguna.

### Output

![](images/play.png)

![](images/play1.png)

### e. 
User juga dapat menambahkan lagu ke dalam playlist dengan syarat sebagai berikut:

User mengirimkan perintah
ADD <SONG1>
ADD <SONG2>
sistem akan menampilkan:
USER <ID_USER> ADD <SONG1>

User dapat mengedit playlist secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-output-kan “SONG ALREADY ON PLAYLIST”

#### Jawaban :

```
void add_song_to_playlist(char *song_name, pid_t user_pid) {
    FILE *playlist_fp = fopen("playlist.txt", "r+");
    if (playlist_fp == NULL) {
        perror("Error: failed to open playlist file");
        return;
    }

    char line[MAX_SIZE];
    while (fgets(line, sizeof(line), playlist_fp)) {
        if (strcasestr(line, song_name)) {
            printf("SONG ALREADY ON PLAYLIST\n");
            fclose(playlist_fp);
            return;
        }
    }

    fseek(playlist_fp, 0, SEEK_END);
    fprintf(playlist_fp, "%s\n", song_name);

    fclose(playlist_fp);

    printf("USER %d ADD %s\n", (int)user_pid, song_name);

    update_playlist();
}
```
**Penjelasan :**
1. `FILE *playlist_fp = fopen("playlist.txt", "r+");` : Membuka file "playlist.txt" dalam mode pembacaan dan penulisan (r+) dan menyimpan file pointer dalam variabel playlist_fp. Jika gagal membuka file, pesan kesalahan akan dicetak dan fungsi akan mengembalikan.
2. `if (playlist_fp == NULL) { ... }` : Memeriksa apakah file pointer playlist_fp bernilai NULL, yang menandakan gagal membuka file. Jika itu terjadi, pesan kesalahan akan dicetak dan fungsi akan mengembalikan.
3. `char line[MAX_SIZE];` : Membuat array karakter line dengan ukuran maksimum MAX_SIZE. MAX_SIZE adalah konstanta yang harus didefinisikan sebelumnya.
4. `while (fgets(line, sizeof(line), playlist_fp)) { ... }` : Membaca setiap baris dalam file playlist_fp menggunakan fungsi fgets. Loop ini akan terus berjalan sampai tidak ada baris lagi yang dapat dibaca. Setiap baris yang dibaca disimpan dalam line.
5. `if (strcasestr(line, song_name)) { ... }` : Memeriksa apakah song_name ditemukan dalam line menggunakan fungsi strcasestr. Fungsi ini melakukan pencarian tanpa memperhatikan huruf besar atau kecil. Jika lagu sudah ada dalam playlist, pesan "SONG ALREADY ON PLAYLIST" akan dicetak, file akan ditutup, dan fungsi akan mengembalikan.
6. `fseek(playlist_fp, 0, SEEK_END);` : Memindahkan posisi file pointer ke akhir file menggunakan fungsi fseek. Ini diperlukan agar penulisan lagu baru dilakukan di akhir file.
7. `fprintf(playlist_fp, "%s\n", song_name);` : Menulis song_name ke file playlist_fp menggunakan fungsi fprintf. %s\n merupakan format string yang digunakan untuk menulis song_name diikuti oleh karakter baris baru.
8. `fclose(playlist_fp);` : Menutup file playlist_fp setelah selesai melakukan operasi penulisan.

```
void update_playlist() {
    const char* filename = "playlist.txt";
    const int MAX_LINES = 1500;
    char* lines[MAX_LINES];
    int num_lines = 0;

    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("Error: Could not open file %s\n", filename);
        return;
    }
    char buffer[100000];
    while (fgets(buffer, sizeof(buffer), file) != NULL) {
        buffer[strcspn(buffer, "\r\n")] = '\0'; 
        lines[num_lines] = strdup(buffer); 
        num_lines++;
    }
    fclose(file);

    qsort(lines, num_lines, sizeof(char*), compare_strings);

    file = fopen(filename, "w");
    if (file == NULL) {
        printf("Error: Could not open file %s\n", filename);
        return;
    }
    for (int i = 0; i < num_lines; i++) {
        fprintf(file, "%s\n", lines[i]);
        free(lines[i]); 
    }
    fclose(file);
}
```
**Penjelasan :**

1. `void update_playlist()` : Mendefinisikan fungsi update_playlist yang tidak mengembalikan nilai (void).
2. `const char* filename = "playlist.txt";` : Mendefinisikan variabel filename sebagai string yang berisi nama file playlist.
3. `const int MAX_LINES = 1500;` : Mendefinisikan konstanta MAX_LINES dengan nilai 1500 yang merupakan batas maksimum jumlah baris lagu dalam playlist.
4. `char* lines[MAX_LINES];` : Mendefinisikan array lines yang berisi pointer ke string, dengan ukuran maksimum MAX_LINES.
5. `FILE* file = fopen(filename, "r");` : Membuka file dengan nama filename dalam mode "r" (membaca). Mengembalikan file pointer yang ditampung dalam variabel file.
6. `char buffer[100000];` : Mendefinisikan array buffer dengan ukuran 100000 yang akan digunakan untuk membaca baris-baris dari file playlist.
7. `while (fgets(buffer, sizeof(buffer), file) != NULL)` : Membaca baris dari file menggunakan fgets dan menyimpannya dalam buffer. Melakukan looping selama masih ada baris yang dapat dibaca dari file.
8. `buffer[strcspn(buffer, "\r\n")] = '\0';` : Menghapus karakter newline (\r dan \n) dari akhir baris yang dibaca dengan menggunakan strcspn dan menggantinya dengan karakter null (\0).
9. `lines[num_lines] = strdup(buffer);` : Mengalokasikan memori untuk string yang disalin dari buffer menggunakan strdup dan menyimpan pointer tersebut ke dalam array lines.
10. `fclose(file);` : Menutup file setelah selesai membacanya.
11. `qsort(lines, num_lines, sizeof(char*), compare_strings);` : Mengurutkan array lines menggunakan fungsi qsort dengan menggunakan compare_strings sebagai fungsi pembanding.
12. `file = fopen(filename, "w");` :  Membuka kembali file dengan mode "w" (menulis). Mengembalikan file pointer yang ditampung dalam variabel file.

### Output

![](images/add.png)

![](images/add1.png)

### f. 
Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.

#### Jawaban :

```
sem_t *sem;
    if ((sem = sem_open("/semaphoresaya", O_CREAT, 0666, MAX_USERS)) == SEM_FAILED) {
        perror("sem_open");
        exit(1);
    }

    printf("Stream is waiting for messages... (\"QUIT\" to exit)\n");

    while (1) {
    //3A sistem stream (receiver) stream.c menggunakan message queue (wajib).
        if (msgrcv(msgid, &message, sizeof(message) - sizeof(long), 0, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }

        int found_user = 0;
        for (int i = 0; i < active_users_count; i++) {
            if (active_users[i] == message.user_pid) {
                found_user = 1;
                break;
            }
        }

        if (!found_user && active_users_count == MAX_USERS) {
            printf("STREAM SYSTEM OVERLOAD: User %d cannot send commands at the moment.\n", message.user_pid);
            continue;
        }

        if (!found_user) {
            if (sem_wait(sem) == -1) {
                perror("sem_wait");
                exit(1);
            }
            active_users[active_users_count++] = message.user_pid;
        }
```
**Penjelasan :**

1. `sem_t *sem;`: Mendeklarasikan pointer sem yang akan digunakan untuk menyimpan alamat semafor.
2. `if ((sem = sem_open("/semaphoresaya", O_CREAT, 0666, MAX_USERS)) == SEM_FAILED)` : Membuka atau membuat semafor dengan menggunakan sem_open. /semaphoresaya adalah nama semafor yang digunakan. Jika pembukaan semafor gagal, maka akan mencetak pesan error menggunakan perror dan program akan keluar menggunakan exit(1).
3. `printf("Stream is waiting for messages... (\"QUIT\" to exit)\n");` : Mencetak pesan bahwa sistem stream sedang menunggu pesan.
4. `while (1)` : Memulai loop tak terbatas.
5. `if (msgrcv(msgid, &message, sizeof(message) - sizeof(long), 0, 0) == -1)` : Menggunakan fungsi msgrcv untuk menerima pesan dari message queue. msgid adalah identifikasi antrean pesan yang digunakan. Jika pemanggilan fungsi msgrcv mengembalikan nilai -1, itu berarti ada kesalahan dan pesan error akan dicetak menggunakan perror. Program akan keluar menggunakan exit(1).
6. `int found_user = 0;` : Mendeklarasikan variabel found_user dengan nilai awal 0.
7. `for (int i = 0; i < active_users_count; i++)` : Memulai loop for untuk mencari pengguna aktif dalam array active_users.
8. `if (active_users[i] == message.user_pid)` : Memeriksa apakah message.user_pid (ID pengguna dari pesan yang diterima) ada di dalam array active_users. Jika ditemukan, variabel found_user diatur menjadi 1 dan loop dihentikan menggunakan break.
9. `if (!found_user && active_users_count == MAX_USERS)` : Memeriksa jika pengguna tidak ditemukan dan jumlah pengguna aktif sudah mencapai batas maksimum (MAX_USERS). Jika kondisi ini terpenuhi, maka mencetak pesan bahwa sistem stream overload, dan program akan melanjutkan ke iterasi berikutnya menggunakan continue.
10. `if (!found_user)` : Memeriksa jika pengguna tidak ditemukan.
11. `if (sem_wait(sem) == -1)` : Menggunakan sem_wait untuk menurunkan nilai semafor. Jika pemanggilan sem_wait mengembalikan nilai -1, itu berarti ada kesalahan dan pesan error akan dicetak menggunakan perror. Program akan keluar menggunakan exit(1).
12. `active_users[active_users_count++] = message.user_pid;` : Menambahkan message.user_pid (ID pengguna dari pesan yang diterima) ke dalam array active_users dan meningkatkan nilai active_users_count.

### g. 
Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

#### Jawaban :

```
else {
            printf("UNKNOWN COMMAND: %s\n", message.message_text);
        }

        if (!found_user) {
            if (sem_post(sem) == -1) {
                perror("sem_post");
                exit(1);
            }
```
**Penjelasan :**

1. `printf("UNKNOWN COMMAND: %s\n", message.message_text);` : Ini adalah pernyataan cetak (print statement) yang mencetak pesan "UNKNOWN COMMAND" beserta isi teks pesan (message_text) ke konsol. %s adalah placeholder untuk menggantikan nilai dari variabel message.message_text.
2. `if (!found_user)` : Ini adalah kondisi if yang memeriksa apakah variabel found_user bernilai false atau tidak. Jika bernilai false, maka blok kode di dalam kondisi if akan dieksekusi.
3. `if (sem_post(sem) == -1)` : Ini adalah pernyataan if yang memeriksa apakah pemanggilan fungsi sem_post(sem) mengembalikan nilai -1 atau tidak. sem_post(sem) adalah pemanggilan fungsi untuk meningkatkan nilai semaphore (sem) dengan 1. Jika pemanggilan fungsi ini mengembalikan nilai -1, maka blok kode di dalam kondisi if akan dieksekusi.
4. `perror("sem_post");` : Jika pemanggilan fungsi sem_post(sem) mengembalikan nilai -1, maka fungsi perror() akan mencetak pesan kesalahan yang terkait dengan fungsi sem_post(). Pesan kesalahan ini akan mencetak pesan "sem_post" ke konsol.
5. `exit(1);` : Ini adalah pernyataan yang menghentikan eksekusi program dengan keluar dari program dan mengembalikan kode keluaran 1.

### Output

![](images/unknown.png)

![](images/unknown1.png)

## No. 4 
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 

### a. 
Download dan unzip file tersebut dalam kode c bernama unzip.c.

#### Jawaban :

```
 char *args[] = {"wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }
```
Penjelasan :

1. `pid_t pid = fork();` : membuat proses baru dan mendapatkan ID proses
2. `if (pid == 0) {` : jika ID proses sama dengan 0, artinya ini adalah child process
3.  char *args[] = {"wget", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
4. `execvp(args[0], args);` : menjalankan perintah wget dengan argumen yang diberikan di atas
5. `exit(EXIT_FAILURE);` : keluar dengan status EXIT_FAILURE jika execvp gagal
6. `} else {` : jika ID proses tidak sama dengan 0, artinya ini adalah parent process
7. `wait(NULL);`} : menunggu child process selesai dieksekusi

```
    // Extract hehe.zip
    pid = fork();
    if (pid == 0) {
        char *args[] = {"unzip", "hehe.zip", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }
```
Penjelasan :

1. `pid = fork();` :  membuat proses baru dan mendapatkan ID proses
2. `if (pid == 0) {` : jika ID proses sama dengan 0, artinya ini adalah child process
3. `char *args[] = {"unzip", "hehe.zip", NULL};` : deklarasi array `args` dengan isi "unzip", "hehe.zip", dan `NULL`.
4. `execvp(args[0], args);` : kode ini digunakan untuk menggantikan proses child saat ini dengan menjalankan perintah `unzip` pada file `hehe.zip`.
5. `exit(EXIT_FAILURE);` : jika perintah `unzip` gagal dieksekusi, maka proses child akan keluar dengan status `EXIT_FAILURE`.
6. `} else {` : jika ID proses tidak sama dengan 0, artinya ini adalah parent process
7. `wait(NULL);}` : menunggu child process selesai dieksekusi
 
```
    // Remove hehe.zip
    pid = fork();
    if (pid == 0) {
        char *args[] = {"rm", "hehe.zip", NULL};
        execvp(args[0], args);
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }
}
```
Penjelasan:

1. `pid = fork();` : membuat proses baru dan mendapatkan ID proses
2. `if (pid == 0) {` : jika ID proses sama dengan 0, artinya ini adalah child process
3. `char *args[] = {"rm", "hehe.zip", NULL};` : mendefinisikan array args yang berisi command "rm" (untuk menghapus file) dan nama file yang akan dihapus ("hehe.zip"), serta nilai NULL sebagai tanda akhir dari array.
4. `execvp(args[0], args);` : menjalankan command "rm" dengan argumen "hehe.zip" pada child process.
5. `exit(EXIT_FAILURE);` : jika execvp gagal, keluar dari child process dengan status EXIT_FAILURE.
6. `} else {` : jika ID proses tidak sama dengan 0, artinya ini adalah parent process.
7. `wait(NULL);}` : menunggu child process selesai dieksekusi.

**Output :**

![4a](images/4a.jpeg)

**Penjelasan Output: **

Output yang dihasilkan berupa :
- Hasil download file hehe.zip dan hasil ekstrak file hehe.zip


### b. 
Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.

#### Jawaban :
```
void* directory(void* arg) { // fungsi untuk membuat direktori
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg; // mendapatkan argumen thread
    char* dirname = thread_args->dirname; // mendapatkan nama direktori
    int maxFile = thread_args->maxFile; // mendapatkan maksimal file yang dapat diproses
    char ext[4]; // string untuk menyimpan ekstensi file
    strcpy(ext, dirname); // menyalin nama direktori ke string ekstensi
    char categorized_dirname[MAX_LEN] = "categorized/"; // string untuk menyimpan nama direktori yang akan dibuat
    char new_dirname[MAX_LEN]; // string untuk menyimpan nama direktori yang akan dibuat
    struct stat st; // struct untuk menyimpan informasi file

    strcpy(new_dirname, categorized_dirname); // menyalin nama direktori ke string nama direktori baru  // membuat nama direktori baru dengan menambahkan prefix "categorized/"
    strcat(new_dirname, dirname); // menambahkan nama direktori ke string nama direktori baru

    //hitung banyak file
    char countFile[256]; // string untuk menyimpan banyak file
    char hehe_dir[20] ="files"; // string untuk menyimpan nama direktori yang akan dihitung
    char cmd[256]; // string untuk menyimpan command
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext); // membuat command untuk menghitung banyak file

    FILE* fp; // file stream untuk membaca output dari command // Mengeksekusi command dan membuka stream untuk membaca outputnya
    fp = popen(cmd, "r"); // Mengeksekusi command dan membuka stream untuk membaca outputnya
    if (fp == NULL) { // Jika gagal mengeksekusi command
        printf("Error: Failed to execute command.\n"); // Menampilkan pesan error
    }

    // Membaca output dari stream dan menampilkannya ke layar
    AccessedLog(hehe_dir); // menulis pesan log (accesed) ketika mengakses direktori
    fgets(countFile, sizeof(countFile), fp); // membaca output dari stream dan menyimpannya ke string countFile
    int numFiles = atoi(countFile); // mengubah string countFile menjadi integer dan menyimpannya ke numFiles

    char _write[MAX_LEN]; // string untuk menyimpan command // ascending output
    system("touch buffer.txt"); // membuat file buffer.txt
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles); // membuat command untuk menulis ke file buffer.txt
    system(_write); // mengeksekusi command untuk menulis ke file buffer.txt
    
    int numDir = numFiles / maxFile ;  //menghitung total direktori yang dibuat per ext [@todo, check if maxFile=0] 

    if (numFiles % maxFile > 0) // jika ada sisa file yang belum diproses
        numDir = numDir + 1; // menambahkan 1 direktori

    pclose(fp); // Menutup stream dan mengembalikan nilai 0 (sukses)

    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) { // membuat direktori jika belum ada  //create dir  if numDir == 0
        MadeLog(new_dirname); // menulis pesan log (made) ketika membuat direktori // directory creation was successful, so do something
    }

    for (int i = 0; i < numDir; i++) // membuat direktori sebanyak numDir
    {
         char _dirName[30]; // string untuk menyimpan nama direktori // mendefinisikan variable nama directory dengan suffix sesuai dengan index
         strcpy(_dirName, new_dirname); // menyalin nama direktori ke string nama direktori baru
         if(i > 0) { // jika index lebih dari 0
            char suffix[10]; // string untuk menyimpan suffix nama direktori
            sprintf(suffix, " (%d)", i+1); // membuat suffix nama direktori
            strcat(_dirName, suffix); // menambahkan suffix ke nama direktori
        }       

        if (stat(_dirName, &st) == 0) { // jika direktori sudah ada // memeriksa apakah direktori sudah ada
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName); // menampilkan pesan ke layar
        } else { // jika direktori belum ada
                if (mkdir(_dirName, 0777) == 0) { // membuat direktori jika belum ada  //create dir  if numDir == 0
                    MadeLog(_dirName); // menulis pesan log (made) ketika membuat direktori // directory creation was successful, so do something
                    //move files according to their extensions
                    char command1[600]; // string untuk menyimpan command 
                    char command2[500]; // string untuk menyimpan command 
                    char command3[100]; // string untuk menyimpan command 
                    char logAccessSource[200]; // string untuk menyimpan command
                    char logAccessTarget[100]; // string untuk menyimpan command 

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext); // membuat command untuk menulis pesan log (accessed) ketika mengakses direktori
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName); // membuat command untuk menulis pesan log (accessed) ketika mengakses direktori
                    
                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName); // membuat command untuk memindahkan file
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s \">> log.txt && %s", ext, _dirName, command3); // membuat command untuk menulis pesan log (moved) ketika memindahkan file
                    sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2); // membuat command untuk memindahkan file
                    
                    system (logAccessSource); // mengeksekusi command untuk menulis pesan log (accessed) ketika mengakses direktori
                    system (logAccessTarget); // mengeksekusi command untuk menulis pesan log (accessed) ketika mengakses direktori

                    system(command1); // mengeksekusi command untuk memindahkan file

                } else { // jika gagal membuat direktori
                    perror("Gagal membuat direktori"); // menampilkan pesan error
                    
                }
            }
    }
    return NULL; // mengembalikan nilai NULL
}
```
Penjelasan :

    Fungsi "directory" adalah sebuah fungsi yang digunakan untuk membuat direktori, memindahkan file-file dengan ekstensi tertentu ke dalam direktori yang sesuai dengan ekstensinya, serta melakukan logging pada saat membuat direktori atau mengakses direktori.

    Pada awal fungsi, akan dilakukan pengambilan argumen-argumen thread, yaitu nama direktori, maksimal file yang dapat diproses, dan ekstensi file. Selanjutnya, akan dibuat string untuk menyimpan nama direktori baru dengan menambahkan prefix "categorized/" pada string "categorized_dirname". Selanjutnya akan dilakukan perhitungan banyak file dengan ekstensi yang ditentukan dengan menghitung output command "find" dan "wc -l", dimana hasil output akan disimpan dalam string "countFile". Selanjutnya, string "extension_" + "ext" + " : " + "numFiles" akan disimpan ke dalam file "buffer.txt" untuk logging ascending output.

    Setelah itu, akan dilakukan penghitungan jumlah direktori yang harus dibuat dengan membagi banyak file dengan maksimal file yang dapat diproses. Kemudian, akan dilakukan pengambilan sisa file jika ada, dan akan ditambahkan satu direktori. Setelah itu, akan dilakukan perulangan untuk membuat direktori sebanyak jumlah direktori yang dihitung tadi. Nama direktori akan diberi suffix sesuai dengan indeks direktori yang sedang dibuat.

    Selanjutnya, akan dilakukan pengecekan apakah direktori sudah ada atau belum. Jika direktori belum ada, maka akan dibuat direktori baru dengan menggunakan perintah "mkdir". Selanjutnya, file-file dengan ekstensi yang sama akan dipindahkan ke dalam direktori yang sesuai dengan ekstensinya menggunakan perintah "mv". Kemudian, akan dilakukan logging dengan menuliskan pesan "made" pada saat membuat direktori dan "accessed" pada saat mengakses direktori.

*Keterangan : Selain untuk menjawab soal poin b, Fungsi "directory" tersebut juga digunakan untuk menjawab soal poin c (meng output-kan banyak file tiap extension terurut ascending), d (pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading.), dan e (membuat log dalam setiap pengaksesan folder, pemindahan file, dan pembuatan folder).

**Output :**

![4b](images/4b.jpeg)

**Penjelasan Output: **

Output yang dihasilkan berupa :
- Pengkategorian files kedalam folder yang berdasarkan extentionnya dan dikumpulkan pada folder "categorized".

### c. 
Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
    extension_a : banyak_file
    extension_b : banyak_file
    extension_c : banyak_file
    other : banyak_file

#### Jawaban :
```
   log_file = fopen("log.txt", "a"); // membuka file log.txt dengan mode append
    if (log_file == NULL) { // jika gagal membuka file
        perror("Failed to open log file"); // menampilkan pesan error
        exit(1); // keluar dari program
    }

    if (pthread_mutex_init(&lock, NULL) != 0) { // inisialisasi mutex lock // jika gagal menginisialisasi mutex lock
        perror("Failed to initialize mutex lock"); // menampilkan pesan error
        exit(1); // keluar dari program
    }

//MEMBUAT CATEGORIZE DIRECTORY
    char filename[50] = "extensions.txt"; // string untuk menyimpan nama file ekstensi
    char dirname[MAX_LEN]; // string untuk menyimpan nama direktori 
    char other_dirname[MAX_LEN] = "other"; // string untuk menyimpan nama direktori
    FILE *fp; // pointer untuk menyimpan file
    struct stat st; // struct untuk menyimpan informasi file
    pthread_t tid[MAX_LEN]; // array untuk menyimpan thread id
    struct ThreadArgs thread_args[MAX_LEN]; // array untuk menyimpan argumen thread
    int i = 0; // inisialisasi index

    fp = fopen(filename, "r"); // membuka file extensions.txt dengan mode read

    // memeriksa apakah file berhasil dibuka
    if (fp == NULL) { // jika gagal membuka file
        printf("File tidak dapat dibuka.\n"); // menampilkan pesan error
        exit(1); // keluar dari program
    }

    // membuat direktori "categorized" jika belum ada
    if (stat("categorized", &st) != 0) { // memeriksa apakah direktori sudah ada
        mkdir("categorized", 0777); // membuat direktori categorized
        MadeLog("categorized"); // menulis pesan log (made) ketika membuat direktori
    }

    // membuat direktori "other" di dalam "categorized" jika belum ada
    if (stat("categorized/other", &st) != 0) { // memeriksa apakah direktori sudah ada
        MadeLog("categorized/other"); // menulis pesan log (made) ketika membuat direktori
        if (mkdir("categorized/other", 0777) != 0) // membuat direktori other di dalam categorized
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n"); // menampilkan pesan error
    }

    char maxfilename[50] = "max.txt"; // string untuk menyimpan nama file max.txt
    FILE *max; // pointer untuk menyimpan file //open max.txt
    max = fopen(maxfilename, "r"); // membuka file max.txt dengan mode read

    // memeriksa apakah file berhasil dibuka
    if (max == NULL) { // jika gagal membuka file
        printf("File tidak dapat dibuka.\n"); // menampilkan pesan error
        exit(1); // keluar dari program
    }

    int maxFile; // integer untuk menyimpan nilai maksimum file
    fscanf(max, "%d", &maxFile); // membaca nilai maksimum file dari file
    fclose(max); // menutup file max.txt

    while (fgets(dirname, MAX_LEN, fp)) { // membaca nama direktori dari file, satu per satu 
        dirname[strcspn(dirname, "\n")] = 0; // menghilangkan karakter newline di akhir string

        // menghapus spasi di akhir string
        size_t len = strlen(dirname); // menghitung panjang string dirname
        while (len > 0 && isspace(dirname[len - 1])) { // jika karakter terakhir adalah spasi
            len--; // mengurangi panjang string
        }
        dirname[len] = '\0'; // mengakhiri string dengan null

        // membuat direktori lain secara asynchronous dengan menggunakan thread
        strcpy(thread_args[i].dirname, dirname); // menyalin nama direktori ke dalam argumen thread
        thread_args[i].maxFile = maxFile; // menyalin nilai maksimum file ke dalam argumen thread
        pthread_create(&tid[i], NULL, directory, &thread_args[i]); // membuat thread
        i++; // menambahkan index
    }

    for (int j = 0; j < i; j++) { // iterasi sebanyak jumlah thread yang dibuat  // join thread yang masih berjalan
        pthread_join(tid[j], NULL); // join thread
    }

    // move other files to categorized/other
    char cmdOther1[600]; // string untuk menyimpan command
    char cmdOther2[500]; // string untuk menyimpan command
    char cmdOther3[100]; // string untuk menyimpan command
    char otherPath[20] = "categorized/other"; // string untuk menyimpan path direktori other
    char logAccessSource[200]; // string untuk menyimpan command
    char logAccessTarget[100]; // string untuk menyimpan command
```

Penjelasan :

Bagian dari fungsi "main" tersebut terdiri dari beberapa langkah untuk melakukan proses categorize directory pada file-file yang ada di dalam folder. Berikut ini penjelasan mengenai apa saja yang dilakukan dalam kode tersebut:

1. Membuka file log.txt dengan mode append, jika gagal membuka file akan menampilkan pesan error dan keluar dari program.
2. Inisialisasi mutex lock, jika gagal akan menampilkan pesan error dan keluar dari program.
3. Membaca file ekstensi dari file "extensions.txt", memeriksa apakah file berhasil dibuka. Jika gagal membuka file akan menampilkan pesan error dan keluar dari program.
4. Membuat direktori "categorized" jika belum ada, dengan memeriksa apakah direktori sudah ada. Jika belum ada, akan membuat direktori "categorized" dan menulis pesan log "made".
5. Membuat direktori "other" di dalam "categorized" jika belum ada, dengan memeriksa apakah direktori sudah ada. Jika belum ada, akan membuat direktori "other" di dalam "categorized" dan menulis pesan log "made". Jika gagal membuat direktori "other" akan menampilkan pesan error.
6. Membaca nilai maksimum file dari file "max.txt", memeriksa apakah file berhasil dibuka. Jika gagal membuka file akan menampilkan pesan error dan keluar dari program.
7. Menginisialisasi beberapa variabel yang akan digunakan, seperti variabel untuk menyimpan nama direktori, variabel untuk menyimpan nilai maksimum file, dan beberapa variabel string lainnya.
8. Membaca nama direktori dari file "extensions.txt", satu per satu, dan menghilangkan karakter newline di akhir string. Kemudian, menghapus spasi di akhir string. Setelah itu, membuat direktori lain secara asynchronous dengan menggunakan thread dan menyalin nama direktori serta nilai maksimum file ke dalam argumen thread.
9. Menunggu thread-thread yang dibuat selesai bekerja dengan menggunakan fungsi pthread_join().
10. Memindahkan file-file yang tidak tercategorize ke dalam folder "categorized/other", dengan mengisi beberapa variabel string command dan menggunakan fungsi system() untuk mengeksekusinya.

Dengan begitu, kode tersebut akan memindahkan file-file ke dalam folder yang sesuai dengan ekstensinya. File-file yang tidak memiliki ekstensi akan dipindahkan ke dalam folder "categorized/other". Selain itu, kode tersebut juga akan menulis log file ketika membuat folder baru atau mengakses suatu file(poin e).

*Keterangan : Selain untuk menjawab soal poin b, Fungsi "directory" tersebut juga digunakan untuk menjawab soal poin c (meng output-kan banyak file tiap extension terurut ascending), d (pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading.), dan e (membuat log dalam setiap pengaksesan folder, pemindahan file, dan pembuatan folder).

**Output :**

![4c](images/4c.jpeg)

**Penjelasan Output: **

Output yang dihasilkan berupa :
- Output pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga.

### d. 
Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

#### Jawaban :
Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Multithreading digunakan untuk membuat direktori baru untuk setiap jenis file yang berbeda dengan ekstensi yang sama. Terdapat beberapa fungsi yang menggunakan mutex untuk mengeksekusi thread secara bersamaan, yakni AccessedLog() dan MadeLog().

Fungsi directory() merupakan fungsi utama untuk membuat direktori baru dengan ekstensi tertentu. Fungsi ini memproses argumen arg yang merupakan struct ThreadArgs yang menyimpan nama direktori yang akan dibuat dan maksimal file yang dapat diproses. Fungsi ini juga menghitung jumlah file yang memiliki ekstensi yang sama dengan menggunakan command "find" dan "wc". Setelah itu, fungsi ini menambahkan prefix "categorized/" pada nama direktori yang akan dibuat, kemudian menyalin nama direktori yang diberikan pada parameter ke dalam string ekstensi.

Untuk setiap ekstensi yang sama, fungsi ini akan membuat sebuah direktori baru dengan nama yang ditentukan dan menuliskan pesan log (made) pada file log. Selanjutnya, fungsi ini akan mengakses setiap file yang memiliki ekstensi yang sama, menyalin file tersebut ke direktori baru yang telah dibuat, dan menuliskan pesan log (accessed) pada file log.

Pada fungsi utama main(), terdapat beberapa thread yang diinisialisasi untuk menjalankan fungsi directory() dengan argumen yang berbeda untuk setiap threadnya. Setelah semua thread selesai dieksekusi, program akan menutup file log dan keluar.


### e. 
Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
    - Path dimulai dari folder files atau categorized
    - Simpan di dalam log.txt
    - ACCESSED merupakan folder files beserta dalamnya
    - Urutan log tidak harus sama

#### Jawaban :

```
void AccessedLog(char folderPath[]){ // fungsi untuk menulis pesan log (accesed) ketika mengakses direktori
    time_t now = time(NULL); // mendapatkan waktu saat ini 
    struct tm *t = localtime(&now); // mendapatkan waktu lokal 
    char logAccess[256] = "ACCESSED "; // pesan log (ACCESSED) yang akan ditulis ke file
    char _time_str[80]; // string untuk menyimpan waktu
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t); // mengubah waktu menjadi string

    strcat(logAccess, folderPath); // menambahkan nama direktori ke pesan log 
    pthread_mutex_lock(&lock); // mengunci mutex agar tidak ada thread lain yang menulis ke file log
    fprintf(log_file, "%s %s\n", _time_str, logAccess); // menulis pesan log ke file
    pthread_mutex_unlock(&lock); // membuka mutex agar thread lain dapat menulis ke file log
}
```
Penjelasan :


Fungsi `AccessedLog` memiliki tujuan untuk menulis pesan log `ACCESSED` ketika suatu direktori diakses. Fungsi ini menerima satu argumen berupa array karakter "folderPath", yang akan ditambahkan ke pesan log.

Di dalam fungsi ini, waktu saat ini diperoleh dengan menggunakan fungsi `time()` dan disimpan dalam variabel `now`. Waktu lokal kemudian diperoleh dengan menggunakan fungsi `localtime()` dan disimpan dalam variabel struct "tm" yang diberi nama "t".

Selanjutnya, variabel `logAccess` berisi pesan log `ACCESSED` yang akan ditulis ke file. Variabel `_time_str` berfungsi untuk menyimpan waktu dalam format string yang akan ditambahkan ke pesan log. Fungsi "strftime()" digunakan untuk mengubah waktu menjadi format string yang diinginkan.

Variabel `folderPath` ditambahkan ke pesan log menggunakan fungsi `strcat()`. Kemudian, mutex `lock` dikunci agar tidak ada thread lain yang dapat menulis ke file log pada saat yang sama. Pesan log kemudian ditulis ke file dengan menggunakan fungsi `fprintf()`. Setelah selesai menulis log, mutex dibuka kembali agar thread lain dapat menulis ke file log.

Dengan menggunakan fungsi ini, kita dapat membuat log file yang merekam setiap kali suatu direktori diakses dengan menambahkan pesan log `ACCESSED` beserta waktu dan path direktori yang diakses.


```
void MadeLog(char createdDir[]){ // fungsi untuk menulis pesan log (made) ketika membuat direktori
    time_t now = time(NULL); // mendapatkan waktu saat ini
    struct tm *t = localtime(&now); // mendapatkan waktu lokal
    char logMADE[256] = "MADE "; // pesan log (MADE) yang akan ditulis ke file
    char _time_str[80]; // string untuk menyimpan waktu
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t); // mengubah waktu menjadi string

    strcat(logMADE, createdDir); // menambahkan nama direktori ke pesan log
    pthread_mutex_lock(&lock); // mengunci mutex agar tidak ada thread lain yang menulis ke file log
    fprintf(log_file, "%s %s\n", _time_str, logMADE); // menulis pesan log ke file
    pthread_mutex_unlock(&lock); // membuka mutex agar thread lain dapat menulis ke file log
}
```
Penjelasan :

Fungsi `MadeLog` adalah sebuah fungsi yang berfungsi untuk menulis pesan log pada file log ketika sebuah direktori baru dibuat. Fungsi ini memiliki satu parameter yaitu `createdDir`, yang merupakan sebuah string yang berisi nama dari direktori yang baru saja dibuat.

Pertama-tama, fungsi ini akan mendapatkan waktu saat ini menggunakan fungsi `time()` dari library `time.h`. Kemudian waktu tersebut akan diubah ke waktu lokal menggunakan fungsi `localtime()`, sehingga kita bisa mendapatkan tanggal dan waktu lokal pada saat pembuatan direktori.

Setelah itu, fungsi ini akan membuat sebuah string `logMADE` yang berisi pesan log "MADE ". Selanjutnya, nama direktori yang baru saja dibuat akan ditambahkan ke string `logMADE` menggunakan fungsi `strcat()`. Kemudian, fungsi ini akan mengunci mutex menggunakan fungsi `pthread_mutex_lock()` agar tidak ada thread lain yang menulis ke file log pada saat yang bersamaan. Pesan log kemudian akan ditulis ke file menggunakan fungsi `fprintf()` dengan format waktu dan pesan log yang telah dibuat sebelumnya. Terakhir, mutex akan dibuka kembali menggunakan fungsi `pthread_mutex_unlock()` agar thread lain dapat menulis ke file log.


```
        if (stat(_dirName, &st) == 0) { // jika direktori sudah ada // memeriksa apakah direktori sudah ada
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName); // menampilkan pesan ke layar
        } else { // jika direktori belum ada
                if (mkdir(_dirName, 0777) == 0) { // membuat direktori jika belum ada  //create dir  if numDir == 0
                    MadeLog(_dirName); // menulis pesan log (made) ketika membuat direktori // directory creation was successful, so do something
                    //move files according to their extensions
                    char command1[600]; // string untuk menyimpan command 
                    char command2[500]; // string untuk menyimpan command 
                    char command3[100]; // string untuk menyimpan command 
                    char logAccessSource[200]; // string untuk menyimpan command
                    char logAccessTarget[100]; // string untuk menyimpan command 

                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext); // membuat command untuk menulis pesan log (accessed) ketika mengakses direktori
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName); // membuat command untuk menulis pesan log (accessed) ketika mengakses direktori
                    
                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName); // membuat command untuk memindahkan file
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s \">> log.txt && %s", ext, _dirName, command3); // membuat command untuk menulis pesan log (moved) ketika memindahkan file
                    sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2); // membuat command untuk memindahkan file
                    
                    system (logAccessSource); // mengeksekusi command untuk menulis pesan log (accessed) ketika mengakses direktori
                    system (logAccessTarget); // mengeksekusi command untuk menulis pesan log (accessed) ketika mengakses direktori

                    system(command1); // mengeksekusi command untuk memindahkan file

                } else { // jika gagal membuat direktori
                    perror("Gagal membuat direktori"); // menampilkan pesan error
                    
                }
            }
```

Penjelasan :

Potongan kode dari fungsi `directory` digunakan untuk melakukan operasi pada direktori dan file yang berkaitan dengan penanganan log ketika direktori diakses, file dipindahkan, dan direktori dibuat.

Pertama, kode memeriksa apakah direktori yang ditentukan sudah ada atau belum dengan menggunakan fungsi `stat()`. Jika direktori sudah ada, maka akan ditampilkan pesan bahwa direktori tersebut sudah ada. Jika belum ada, maka akan dilakukan operasi untuk membuat direktori baru menggunakan fungsi `mkdir()` dengan parameter `dirName` (nama direktori) dan mode akses `0777` (semua akses).

Ketika direktori berhasil dibuat, maka fungsi `MadeLog()` akan dipanggil untuk menulis pesan `log (made)` yang memberikan informasi bahwa direktori baru telah dibuat. Pesan log tersebut akan mencatat waktu saat pembuatan direktori dan nama direktori yang dibuat.

Selanjutnya, kode akan melakukan operasi untuk memindahkan file ke dalam direktori baru. Pertama-tama, akan dibuat sebuah string command untuk menulis pesan `log (accessed)` ketika mengakses direktori lama (yang berisi file yang akan dipindahkan) dan direktori baru. String command tersebut menggunakan perintah find untuk mencari file dengan ekstensi yang sesuai, kemudian menggunakan perintah xargs untuk mengeksekusi perintah shell sh yang akan menuliskan pesan log ke file `log.txt`.

Kemudian, akan dibuat sebuah string command untuk menuliskan pesan log (moved) ketika file dipindahkan ke direktori baru. String command tersebut akan menggunakan perintah echo untuk menuliskan pesan log dan menggunakan perintah mv untuk memindahkan file.

Kemudian, kedua string command tersebut akan digabungkan menjadi sebuah command yang lengkap dengan menggunakan fungsi `sprintf()`. Command tersebut akan dieksekusi menggunakan fungsi `system()`.

Jika gagal membuat direktori, maka akan ditampilkan pesan error menggunakan fungsi `perror()`.


```
 sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt"); // membuat command untuk menulis log
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath); // membuat command untuk menulis log
    
    system(logAccessSource); // menulis log (accessed) ketika mengakses file
    system(logAccessTarget); // menulis log (accessed) ketika mengakses file

    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath); // membuat command untuk memindahkan file ke dalam direktori other
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s \">> log.txt && %s", otherPath, cmdOther3); // membuat command untuk menulis log (moved) dan memindahkan file
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2); // membuat command untuk memindahkan file ke dalam direktori other dan menulis log (moved)
    system(cmdOther1); // memindahkan file ke dalam direktori other dan menulis log (moved)
```
Penjelasan :

Potongan kode dari fungsi `main` tersebut berfungsi untuk menulis log ketika mengakses dan memindahkan file dari suatu direktori ke direktori lain. 

Pada bagian pertama, 
```
sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");  
```
akan membuat command untuk menulis log ketika mengakses file pada direktori "files". Command tersebut akan mencari file pada direktori "files" dengan tipe file tertentu, dan mengeksekusi command echo yang akan menampilkan pesan "ACCESSED" beserta direktori tempat file tersebut berada pada file log. File log akan disimpan pada file "log.txt".

Pada bagian selanjutnya, 
```
sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath); 
```

akan membuat command untuk menulis log ketika mengakses direktori "otherPath". Command tersebut akan mengeksekusi command `echo` yang akan menampilkan pesan "ACCESSED" beserta direktori "otherPath" pada file log yang sama.

Setelah itu, 
```
sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath); 
```
akan membuat command untuk memindahkan file yang diproses ke direktori "otherPath". Command tersebut menggunakan single quotes pada bagian `{}` dan double quotes pada direktori "otherPath" agar file dapat diproses dengan baik oleh shell.

Lalu, 
```
sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s \">> log.txt && %s", otherPath, cmdOther3); 
```
akan membuat command untuk menampilkan pesan "MOVED" beserta nama file yang dipindahkan ke direktori "otherPath" pada file log yang sama. Command tersebut juga akan memindahkan file dengan menggunakan command yang dibuat pada bagian sebelumnya.

Terakhir, 
```
sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2); 
```
akan membuat command untuk mencari file pada direktori "files" dan mengeksekusi command yang dibuat pada bagian sebelumnya untuk memindahkan file ke direktori "otherPath" dan menampilkan pesan "MOVED" pada file log. Command tersebut akan mengeksekusi semua command pada satu baris dengan menggunakan `xargs`.

Kemudian, `system(cmdOther1);` akan mengeksekusi command yang dibuat pada bagian sebelumnya untuk memindahkan file ke direktori "otherPath" dan menampilkan pesan "MOVED" pada file log.

**Output :**
![4e](images/4e.jpeg)

**Penjelasan Output: **
Output yang dihasilkan berupa 
- file log.txt yang isinya adalah log setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c.

### f.
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
    - Untuk menghitung banyaknya ACCESSED yang dilakukan.
    - Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
    - Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.

#### Jawaban :
```
    // Untuk menghitung banyaknya ACCESSED yang dilakukan.
    printf ("Banyaknya ACCESSED yang dilakukan:\n"); // mencetak teks 
    system("grep ACCESSED log.txt | wc -l"); // mengeksekusi perintah grep ACCESSED log.txt | wc -l
```
Penjelasan :
Kode tersebut bertujuan untuk menghitung jumlah kali kata "ACCESSED" muncul dalam sebuah file log.txt menggunakan command-line interface pada sistem operasi. 

Penjelasan baris per baris dari kode tersebut adalah sebagai berikut:

1. printf ("Banyaknya ACCESSED yang dilakukan:\n"); : Fungsi `printf()` digunakan untuk mencetak teks ke layar. Di sini, teks yang dicetak adalah "Banyaknya ACCESSED yang dilakukan:" yang diakhiri dengan karakter newline (`\n`).

2. system("grep ACCESSED log.txt | wc -l"); : Fungsi `system()` digunakan untuk mengeksekusi perintah shell pada sistem operasi. Di sini, perintah yang dijalankan adalah `grep ACCESSED log.txt | wc -l`. 
    - `grep ACCESSED log.txt` digunakan untuk mencari semua baris dalam file `log.txt` yang mengandung kata "ACCESSED".
    - `|` (pipe) digunakan untuk mengalirkan output dari perintah `grep` ke perintah `wc`.
    - `wc -l` digunakan untuk menghitung jumlah baris yang dihasilkan oleh perintah `grep`.

Dengan demikian, keseluruhan kode tersebut akan mencetak jumlah baris dalam file `log.txt` yang mengandung kata "ACCESSED", yang menunjukkan berapa kali kata tersebut muncul dalam file tersebut.


```
    // Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
    printf ("\nList seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan:\n"); // mencetak teks
    // mengeksekusi perintah grep 'MOVED\|MADE' log.txt | awk -F 'categorized' '{print "categorized"$NF}' | sed '/^$/d' | sort | uniq -c | awk '{print $2 " " $3 "=" " " $1-1}'
    system("grep 'MOVED\\|MADE' log.txt \
    | awk -F 'categorized' '{print \"categorized\"$NF}' \
    | sed '/^$/d' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");
```
Penjelasan :
Kode tersebut bertujuan untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, yang diurutkan secara ascending berdasarkan nama folder.

Penjelasan baris per baris dari kode tersebut adalah sebagai berikut:

1. printf ("\nList seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan:\n"); : Fungsi `printf()` digunakan untuk mencetak teks ke layar. Di sini, teks yang dicetak adalah `"List seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan:"` yang diakhiri dengan karakter newline `(`\n`)`.

2. system("grep 'MOVED\\|MADE' log.txt \  : Fungsi `system()` digunakan untuk mengeksekusi perintah shell pada sistem operasi. Di sini, perintah yang dijalankan adalah `grep 'MOVED\\|MADE' log.txt`.
    - `grep 'MOVED\\|MADE' log.txt` digunakan untuk mencari semua baris dalam file `log.txt` yang mengandung kata "MOVED" atau "MADE".

3. | awk -F 'categorized' '{print \"categorized\"$NF}' \  : Operator pipe (`|`) digunakan untuk mengalirkan output dari perintah sebelumnya (`grep`) ke perintah berikutnya (`awk`). 
    - `awk` adalah sebuah program yang digunakan untuk memproses dan melakukan manipulasi pada teks. 
    - `-F 'categorized'` digunakan untuk menentukan delimiter (pemisah) yang digunakan dalam pemrosesan teks. Di sini, delimiter yang digunakan adalah kata "categorized".
    - '{print \"categorized\"$NF}' digunakan untuk mencetak teks "categorized" diikuti dengan bagian terakhir dari baris yang dijadikan input. Bagian terakhir tersebut didapatkan dengan menggunakan variabel `$NF` pada perintah `awk`.

4. `(| sed '/^$/d' \)` : Operator pipe (`|`) digunakan untuk mengalirkan output dari perintah sebelumnya (`awk`) ke perintah berikutnya (`sed`).
    - `sed` digunakan untuk melakukan manipulasi pada teks.
    - `'/^$/d'` digunakan untuk menghapus baris yang kosong dalam output yang dihasilkan oleh perintah sebelumnya.

5. `(| sort \) ` : Operator pipe (`|`) digunakan untuk mengalirkan output dari perintah sebelumnya (`sed`) ke perintah berikutnya (`sort`).
    - `sort` digunakan untuk mengurutkan baris dalam teks secara ascending atau descending berdasarkan urutan karakter.

6. `(| uniq -c \) ` : Operator pipe (`|`) digunakan untuk mengalirkan output dari perintah sebelumnya (`sort`) ke perintah berikutnya (`uniq`).
    - `uniq` digunakan untuk menghapus baris duplikat pada teks.
    - `-c` digunakan untuk menampilkan jumlah baris yang sama pada teks.

7. | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'"); : Operator pipe (`|`) digunakan untuk mengalirkan output dari perintah sebelumnya (`uniq`) ke perintah berikutnya (`awk`).
    - {print $2 \" \"$3 \"=\" \" \" $1-1} digunakan untuk mencetak output dengan format "nama folder" "jumlah file dalam folder" "=" "jumlah folder dikurangi satu" (karena output dari `grep` pada awalnya termasuk baris header).
    - `$2`, `$3`, dan `$1` digunakan untuk mengambil nilai kedua, ketiga, dan pertama dari output perintah `uniq -c`. 
        - `$2` adalah nama folder yang telah di-sort sebelumnya.
        - `$3` adalah tanda sama dengan (=).
        - `$1` adalah jumlah file dalam folder tersebut, yang ditambahkan dengan 1 karena baris header juga dihitung.
    - `" \""` digunakan untuk menyisipkan spasi di antara string yang akan dicetak.
    - `\"` digunakan untuk mencetak tanda kutip ganda secara literal.

Dengan cara ini, output dari kode tersebut akan menampilkan daftar folder dan jumlah file dalam folder tersebut, yang diurutkan berdasarkan nama folder secara ascending.


```
    // Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.
    printf ("\nBanyaknya total file tiap extension:\n");
    // mengeksekusi perintah grep 'MADE\|MOVED' log.txt | grep -o 'categorized/[^o]' | sed 's/categorized\///' | sed '/^$/d' | sed 's/ \([^o]\)//' | sort | uniq -c | awk '{print $2 " " "=" " " $1-1}'
    system("grep 'MADE\\|MOVED' log.txt \
    | grep -o 'categorized/[^o]*' \
    | sed 's/categorized\\///' \
    | sed '/^$/d' \
    | sed 's/ \\([^o]*\\)//' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \" \"=\" \" \" $1-1}'");
```
Penjelasan :
Kode di atas digunakan untuk menghitung banyaknya total file tiap extension yang telah dibuat atau dipindahkan ke folder tertentu dan menampilkan hasilnya secara ascending. Berikut ini adalah penjelasan masing-masing perintah yang digunakan pada kode tersebut:

1. printf ("\nBanyaknya total file tiap extension:\n"); : Digunakan untuk mencetak teks `"Banyaknya total file tiap extension:"` pada layar.

2. system("grep 'MADE\\|MOVED' log.txt \: Mengambil semua baris dari file `log.txt` yang mengandung string `"MADE"` atau `"MOVED"`. 

3. | grep -o 'categorized/[^o]*' \: Mengambil string "categorized" beserta path file yang ada di belakangnya dan disimpan ke dalam output. Perintah ini juga melakukan filtering untuk string yang tidak mengandung karakter `"o"` setelah `"/categorized/"`.

4. | sed 's/categorized\\///' \: Menghilangkan string "categorized/" yang ada di output dari perintah sebelumnya.

5. | sed '/^$/d' \: Menghapus baris yang kosong dari output perintah sebelumnya.

6. | sed 's/ \\([^o]*\\)//' \ : Menghapus semua karakter setelah spasi (termasuk spasi itu sendiri) dan karakter "o" dari setiap baris output dari perintah sebelumnya.

7. | sort \ : Mengurutkan baris output secara ascending.

8. | uniq -c \: Menghitung jumlah kemunculan setiap baris output dan menyertakan jumlahnya pada awal baris.

9. | awk '{print $2 \" \" \"=\" \" \" $1-1}'"); : Mengambil nilai kedua dan pertama dari setiap baris output dari perintah sebelumnya dan mencetaknya dengan format "nama ekstensi" "=" "jumlah file yang memiliki ekstensi tersebut dikurangi satu" (karena baris header juga dihitung). 
   - `$2` adalah nama ekstensi file yang telah di-sort sebelumnya.
   - `$1` adalah jumlah file dengan ekstensi tersebut, yang ditambahkan dengan 1 karena baris header juga dihitung.
   - `" \""` digunakan untuk menyisipkan spasi di antara string yang akan dicetak.
   - `\"` digunakan untuk mencetak tanda kutip ganda secara literal.

Dengan cara ini, output dari kode tersebut akan menampilkan daftar ekstensi file dan jumlah file dengan ekstensi tersebut, yang diurutkan berdasarkan nama ekstensi secara ascending.

**Output :**

![4f](images/4f.jpeg)

**Penjelasan Output :**

Output yang dihasilkan berupa :
- banyaknya ACCESSED yang dilakukan, 
- list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
- banyaknya total file tiap extension, terurut secara ascending.

## Kendala
- No 1 : 
awalnya kesulitan pada huffman codesnya, tetapi  bisa menemukannya pada akhirnya.
- No 2 :
bingung kenapa hasilnya gabisa benar kalau lebih dari 20!, ternyata kapasitasnya melebih yang dimiliki int dan unsigned long long int.
- No 3 :
1. ada beberapa jenis parse json sehingga membuat bingung dikarenakan ada beberapa cara yang tidak work
2. cara decrypt kadang membingungkan
3. perintah dalam modul beberapa sulit dipahami

- No 4 : 
1. Terdapat kesulitan dalam pengelompokan file kedalam folder-folder berdasarkan extentionnya. Kode yang dibuat terus menerus mengalami error dan sulit untuk mendapatkan bagian mana yang errornya.
2. Ketika proses pengelompokan file kedalam folder-folder extention, file hanya dipindahkan kedalam file other dan menambahkan beberapa folder extention, setelahnya muncul peringatan segmentation fault (core dumped)"

